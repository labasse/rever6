﻿using Newtonsoft.Json;
using Rever6Forms.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Rever6Forms
{
    public partial class FrmMain : Form
    {
        private const int BoardSize = 8;
        private const string Server = "https://localhost:44384";
        private readonly FrmAbout dlgAbout = new FrmAbout();
        private readonly FrmCreateJoin dlgCreateJoin = new FrmCreateJoin(Server);
        private readonly Label[,] labels = new Label[BoardSize, BoardSize];
        private int MyPlayerNum = 1;
        private OnlineGame LatestGame = null;

        public FrmMain()
        {
            InitializeComponent();
        }

        private void FrmMain_Load(object sender, EventArgs e)
        {
            var font = new Font(SystemFonts.DefaultFont.FontFamily, 24, FontStyle.Regular, GraphicsUnit.Pixel);

            for (var row = 0; row < BoardSize; row++)
            {
                for (var col = 0; col < BoardSize; col++)
                {
                    var pos = new Point(col, row);
                    var label = new Label();

                    label.Dock = DockStyle.Fill;
                    label.TextAlign = ContentAlignment.MiddleCenter;
                    label.Text = String.Empty;
                    label.Font = font;
                    label.ImageAlign = ContentAlignment.MiddleCenter;
                    label.ImageList = imageListGame;
                    label.ImageIndex = -1;
                    label.Tag = null;
                    label.Click += Tile_Click;
                    labels[col, row] = label;
                    tableBoard.Controls.Add(label, col, row);
                }
            }
            miServerCreateJoin_Click(miServerCreateJoin, EventArgs.Empty);
        }

        private void miServerCreateJoin_Click(object sender, EventArgs e)
        {
            Log("miServerCreateJoin_Click");
            if (dlgCreateJoin.ShowDialog(this) == DialogResult.OK)
            {
                tooltipCnct.Text = dlgCreateJoin.GameName;
                timer_Tick(timer, EventArgs.Empty);
                timer.Start();
            }
        }

        private void miServerQuit_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void miHelpAbout_Click(object sender, EventArgs e)
        {
            dlgAbout.ShowDialog(this);
        }

        private string GameUrl => $"{ Server }/games/{dlgCreateJoin.GameName}";

        private async void timer_Tick(object sender, EventArgs e)
        {
            try
            {
                using (var http = new HttpClient())
                {
                    ProcessReceivedGame(JsonConvert.DeserializeObject<OnlineGame>(
                        await http.GetStringAsync(GameUrl)
                    ));
                    Log($"[API] GET { GameUrl } : Réussi");
                }
            }
            catch (HttpRequestException ex)
            {
                tooltipInfo.Text = $@"/!\ Erreur serveur : { ex.Message }";
            }
            catch (JsonException ex)
            {
                tooltipInfo.Text = $@"/!\ Erreur données : { ex.Message }";
            }
        }
        private async void Tile_Click(object sender, EventArgs e)
        {
            var label = sender as Label;

            if (label == null || label.Tag == null)
            {
                return;
            }
            Log("Tile_Click()");
            Cursor = Cursors.WaitCursor;
            try
            {
                Log($"[API] PATCH { GameUrl }");
                using (var http = new HttpClient())
                {
                    var play = label.Tag as OnlinePlayLocation;
                    var request = new HttpRequestMessage(new HttpMethod("PATCH"), GameUrl);
                    var content = JsonConvert.SerializeObject(play.Pos);

                    Log($"- Donnée sérialisée : { content }");
                    request.Content = new StringContent(
                        content,
                        System.Text.Encoding.UTF8,
                        "application/json"
                    );
                    var response = await http.SendAsync(request);

                    if (response.StatusCode == HttpStatusCode.OK)
                    {
                        ProcessReceivedGame(JsonConvert.DeserializeObject<OnlineGame>(
                            await response.Content.ReadAsStringAsync()
                        ));
                        Log($"[API] PATCH { GameUrl } : Nouveau game reçu");
                    }
                    else
                    {
                        Log($"[API] PATCH { GameUrl } : Rejeté");
                        MessageBox.Show(this, response.ReasonPhrase, "Proposition refusée", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }
            }
            catch (HttpRequestException ex)
            {
                MessageBox.Show(this, ex.Message, "Envoi de la proposition", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            catch (JsonException ex)
            {
                MessageBox.Show(this, ex.Message, "Données reçues", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                Cursor = Cursors.Default;
            }
        }
        private void ProcessReceivedGame(OnlineGame game)
        {
            if (LatestGame == null || !game.Tiles.SequenceEqual(LatestGame.Tiles))
            {
                Log($"ProcessReceivedGame() : Jeu modifié");
                LatestGame = game;
                UpdateBoard();
                ProcessEndGame();
            }
        }
        private void UpdateBoard()
        {
            Log($"UpdateBoard()");
            tooltipInfo.Text = "Récupération des données de jeu";
            SuspendLayout();
            try
            {
                Cursor cursor = UpdateCurPlayer();
                var col = 0;
                var row = 0;

                foreach (var tile in LatestGame.Tiles)
                {
                    var label = labels[col, row];

                    label.Text = "";
                    label.BackColor = Color.Transparent;
                    label.ImageIndex = (tile + 3) % 3 - 1;
                    label.Cursor = cursor;
                    label.Tag = null;
                    if (++col >= BoardSize)
                    {
                        row++;
                        col = 0;
                    }
                }
                ProcessPlayableTiles();
            }
            finally
            {
                ResumeLayout();
            }            
        }
        private Cursor UpdateCurPlayer()
        {
            Log($"UpdateCurPlayer()");
            if (!LatestGame.CurPlayer.HasValue)
            {
                timer.Stop();
                return Cursors.Default;
            }
            tooltipColor.BackColor = LatestGame.CurPlayer.Value == -1
                ? Color.Blue
                : Color.Red;
            if (LatestGame.CurPlayer == MyPlayerNum)
            {
                tooltipInfo.Text = "A vous de jouer !";
                return Cursors.No;
            }
            else
            {
                tooltipInfo.Text = "Merci d'attendre l'autre joueur";
                return Cursors.WaitCursor;
            }
        }
        private void ProcessPlayableTiles()
        {
            if (LatestGame.CurPlayer != MyPlayerNum)
            {
                return;
            }
            Log($"ProcessPlayableTiles()");
            foreach (var possibility in LatestGame.CurPlayerPossibilities)
            {
                var label = labels[possibility.Pos.Column, possibility.Pos.Row];

                label.Cursor = Cursors.Hand;
                label.Tag = possibility;
                label.BackColor = miGameTileOk.Checked ? Color.LightGreen : Color.Transparent;
                label.Text = miGameScores.Checked ? possibility.Score : "";
            }
        }

        private void miGameTileOk_Click(object sender, EventArgs e) => ProcessPlayableTiles();

        private void miGameColor_Click(object sender, EventArgs e)
        {
            var menuItem = sender as ToolStripMenuItem;

            MyPlayerNum = (int)menuItem.Tag;
            miGameRed.Checked = MyPlayerNum == 1;
            miGameBlue.Checked = MyPlayerNum == -1;
            UpdateBoard();
        }
        private void ProcessEndGame()
        {
            if (LatestGame.CurPlayer.HasValue)
            {
                return;
            }
            Log($"ProcessEndGame()");
            string message;

            tooltipInfo.Text = "Sélectionnez Serveur > Joindre... pour une autre partie.";
            tooltipColor.Text = "---";
            tooltipColor.BackColor = Color.Transparent;
            timer.Stop();
            if (!LatestGame.Winner.HasValue)
            {
                message = "Match nul.";
            }
            else if (LatestGame.Winner == MyPlayerNum)
            {
                message = "Vous gagnez ! Bravo !";
            }
            else
            {
                message = "Vous perdez, désolé.";
            }
            MessageBox.Show(message, "Fin de partie", MessageBoxButtons.OK);
        }
        private void Log(string s)
        {
            lbLogs.Items.Add(s);
        }
    }
}
