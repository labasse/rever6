﻿namespace Rever6Forms
{
    partial class FrmMain
    {
        /// <summary>
        /// Variable nécessaire au concepteur.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Nettoyage des ressources utilisées.
        /// </summary>
        /// <param name="disposing">true si les ressources managées doivent être supprimées ; sinon, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Code généré par le Concepteur Windows Form

        /// <summary>
        /// Méthode requise pour la prise en charge du concepteur - ne modifiez pas
        /// le contenu de cette méthode avec l'éditeur de code.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmMain));
            this.menuStrip = new System.Windows.Forms.MenuStrip();
            this.miServer = new System.Windows.Forms.ToolStripMenuItem();
            this.miServerCreateJoin = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem1 = new System.Windows.Forms.ToolStripSeparator();
            this.miServerQuit = new System.Windows.Forms.ToolStripMenuItem();
            this.miGame = new System.Windows.Forms.ToolStripMenuItem();
            this.miGameRed = new System.Windows.Forms.ToolStripMenuItem();
            this.miGameBlue = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem2 = new System.Windows.Forms.ToolStripSeparator();
            this.miGameTileOk = new System.Windows.Forms.ToolStripMenuItem();
            this.miGameScores = new System.Windows.Forms.ToolStripMenuItem();
            this.miHelp = new System.Windows.Forms.ToolStripMenuItem();
            this.miHelpAbout = new System.Windows.Forms.ToolStripMenuItem();
            this.statusStrip = new System.Windows.Forms.StatusStrip();
            this.tooltipInfo = new System.Windows.Forms.ToolStripStatusLabel();
            this.tooltipColor = new System.Windows.Forms.ToolStripStatusLabel();
            this.tooltipCnct = new System.Windows.Forms.ToolStripStatusLabel();
            this.timer = new System.Windows.Forms.Timer(this.components);
            this.tableBoard = new System.Windows.Forms.TableLayoutPanel();
            this.imageListGame = new System.Windows.Forms.ImageList(this.components);
            this.lbLogs = new System.Windows.Forms.ListBox();
            this.menuStrip.SuspendLayout();
            this.statusStrip.SuspendLayout();
            this.SuspendLayout();
            // 
            // menuStrip
            // 
            this.menuStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.miServer,
            this.miGame,
            this.miHelp});
            this.menuStrip.Location = new System.Drawing.Point(0, 0);
            this.menuStrip.Name = "menuStrip";
            this.menuStrip.Size = new System.Drawing.Size(720, 24);
            this.menuStrip.TabIndex = 0;
            this.menuStrip.Text = "menuStrip1";
            // 
            // miServer
            // 
            this.miServer.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.miServerCreateJoin,
            this.toolStripMenuItem1,
            this.miServerQuit});
            this.miServer.Name = "miServer";
            this.miServer.Size = new System.Drawing.Size(58, 20);
            this.miServer.Text = "&Serveur";
            // 
            // miServerCreateJoin
            // 
            this.miServerCreateJoin.Name = "miServerCreateJoin";
            this.miServerCreateJoin.Size = new System.Drawing.Size(121, 22);
            this.miServerCreateJoin.Text = "Joindre...";
            this.miServerCreateJoin.Click += new System.EventHandler(this.miServerCreateJoin_Click);
            // 
            // toolStripMenuItem1
            // 
            this.toolStripMenuItem1.Name = "toolStripMenuItem1";
            this.toolStripMenuItem1.Size = new System.Drawing.Size(118, 6);
            // 
            // miServerQuit
            // 
            this.miServerQuit.Name = "miServerQuit";
            this.miServerQuit.Size = new System.Drawing.Size(121, 22);
            this.miServerQuit.Text = "&Quitter";
            this.miServerQuit.Click += new System.EventHandler(this.miServerQuit_Click);
            // 
            // miGame
            // 
            this.miGame.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.miGameRed,
            this.miGameBlue,
            this.toolStripMenuItem2,
            this.miGameTileOk,
            this.miGameScores});
            this.miGame.Name = "miGame";
            this.miGame.Size = new System.Drawing.Size(49, 20);
            this.miGame.Text = "&Partie";
            // 
            // miGameRed
            // 
            this.miGameRed.Checked = true;
            this.miGameRed.CheckState = System.Windows.Forms.CheckState.Checked;
            this.miGameRed.Name = "miGameRed";
            this.miGameRed.Size = new System.Drawing.Size(180, 22);
            this.miGameRed.Tag = 1;
            this.miGameRed.Text = "&Rouge";
            this.miGameRed.Click += new System.EventHandler(this.miGameColor_Click);
            // 
            // miGameBlue
            // 
            this.miGameBlue.Name = "miGameBlue";
            this.miGameBlue.Size = new System.Drawing.Size(180, 22);
            this.miGameBlue.Tag = -1;
            this.miGameBlue.Text = "&Bleu";
            this.miGameBlue.Click += new System.EventHandler(this.miGameColor_Click);
            // 
            // toolStripMenuItem2
            // 
            this.toolStripMenuItem2.Name = "toolStripMenuItem2";
            this.toolStripMenuItem2.Size = new System.Drawing.Size(177, 6);
            // 
            // miGameTileOk
            // 
            this.miGameTileOk.Checked = true;
            this.miGameTileOk.CheckOnClick = true;
            this.miGameTileOk.CheckState = System.Windows.Forms.CheckState.Checked;
            this.miGameTileOk.Name = "miGameTileOk";
            this.miGameTileOk.Size = new System.Drawing.Size(180, 22);
            this.miGameTileOk.Text = "&Cases accessibles";
            this.miGameTileOk.Click += new System.EventHandler(this.miGameTileOk_Click);
            // 
            // miGameScores
            // 
            this.miGameScores.Checked = true;
            this.miGameScores.CheckOnClick = true;
            this.miGameScores.CheckState = System.Windows.Forms.CheckState.Checked;
            this.miGameScores.Name = "miGameScores";
            this.miGameScores.Size = new System.Drawing.Size(180, 22);
            this.miGameScores.Text = "&Scores possibles";
            this.miGameScores.Click += new System.EventHandler(this.miGameTileOk_Click);
            // 
            // miHelp
            // 
            this.miHelp.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.miHelpAbout});
            this.miHelp.Name = "miHelp";
            this.miHelp.Size = new System.Drawing.Size(24, 20);
            this.miHelp.Text = "&?";
            // 
            // miHelpAbout
            // 
            this.miHelpAbout.Name = "miHelpAbout";
            this.miHelpAbout.Size = new System.Drawing.Size(131, 22);
            this.miHelpAbout.Text = "&A propos...";
            this.miHelpAbout.Click += new System.EventHandler(this.miHelpAbout_Click);
            // 
            // statusStrip
            // 
            this.statusStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tooltipInfo,
            this.tooltipColor,
            this.tooltipCnct});
            this.statusStrip.Location = new System.Drawing.Point(0, 576);
            this.statusStrip.Name = "statusStrip";
            this.statusStrip.Size = new System.Drawing.Size(720, 22);
            this.statusStrip.TabIndex = 1;
            this.statusStrip.Text = "statusStrip1";
            // 
            // tooltipInfo
            // 
            this.tooltipInfo.Name = "tooltipInfo";
            this.tooltipInfo.Size = new System.Drawing.Size(601, 17);
            this.tooltipInfo.Spring = true;
            this.tooltipInfo.Text = "Connectez-vous pour jouer : Serveur > Joindre...";
            this.tooltipInfo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // tooltipColor
            // 
            this.tooltipColor.Name = "tooltipColor";
            this.tooltipColor.Size = new System.Drawing.Size(22, 17);
            this.tooltipColor.Text = "---";
            // 
            // tooltipCnct
            // 
            this.tooltipCnct.Name = "tooltipCnct";
            this.tooltipCnct.Size = new System.Drawing.Size(82, 17);
            this.tooltipCnct.Text = "Non connecté";
            // 
            // timer
            // 
            this.timer.Interval = 2000;
            this.timer.Tick += new System.EventHandler(this.timer_Tick);
            // 
            // tableBoard
            // 
            this.tableBoard.ColumnCount = 8;
            this.tableBoard.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 12.5F));
            this.tableBoard.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 12.5F));
            this.tableBoard.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 12.5F));
            this.tableBoard.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 12.5F));
            this.tableBoard.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 12.5F));
            this.tableBoard.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 12.5F));
            this.tableBoard.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 12.5F));
            this.tableBoard.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 12.5F));
            this.tableBoard.Dock = System.Windows.Forms.DockStyle.Top;
            this.tableBoard.Location = new System.Drawing.Point(0, 24);
            this.tableBoard.Name = "tableBoard";
            this.tableBoard.RowCount = 8;
            this.tableBoard.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 12.5F));
            this.tableBoard.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 12.5F));
            this.tableBoard.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 12.5F));
            this.tableBoard.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 12.5F));
            this.tableBoard.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 12.5F));
            this.tableBoard.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 12.5F));
            this.tableBoard.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 12.5F));
            this.tableBoard.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 12.5F));
            this.tableBoard.Size = new System.Drawing.Size(720, 552);
            this.tableBoard.TabIndex = 2;
            // 
            // imageListGame
            // 
            this.imageListGame.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageListGame.ImageStream")));
            this.imageListGame.TransparentColor = System.Drawing.Color.Transparent;
            this.imageListGame.Images.SetKeyName(0, "red.png");
            this.imageListGame.Images.SetKeyName(1, "blue.png");
            // 
            // lbLogs
            // 
            this.lbLogs.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lbLogs.Font = new System.Drawing.Font("Courier New", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbLogs.FormattingEnabled = true;
            this.lbLogs.ItemHeight = 14;
            this.lbLogs.Location = new System.Drawing.Point(0, 576);
            this.lbLogs.Name = "lbLogs";
            this.lbLogs.Size = new System.Drawing.Size(720, 0);
            this.lbLogs.TabIndex = 3;
            // 
            // FrmMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(720, 598);
            this.Controls.Add(this.lbLogs);
            this.Controls.Add(this.tableBoard);
            this.Controls.Add(this.statusStrip);
            this.Controls.Add(this.menuStrip);
            this.MainMenuStrip = this.menuStrip;
            this.MaximumSize = new System.Drawing.Size(736, 800);
            this.MinimumSize = new System.Drawing.Size(736, 637);
            this.Name = "FrmMain";
            this.Text = "Rever6";
            this.Load += new System.EventHandler(this.FrmMain_Load);
            this.menuStrip.ResumeLayout(false);
            this.menuStrip.PerformLayout();
            this.statusStrip.ResumeLayout(false);
            this.statusStrip.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStrip;
        private System.Windows.Forms.ToolStripMenuItem miServer;
        private System.Windows.Forms.ToolStripMenuItem miServerCreateJoin;
        private System.Windows.Forms.ToolStripSeparator toolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem miServerQuit;
        private System.Windows.Forms.ToolStripMenuItem miGame;
        private System.Windows.Forms.ToolStripMenuItem miGameRed;
        private System.Windows.Forms.ToolStripMenuItem miGameBlue;
        private System.Windows.Forms.ToolStripMenuItem miHelp;
        private System.Windows.Forms.ToolStripMenuItem miHelpAbout;
        private System.Windows.Forms.StatusStrip statusStrip;
        private System.Windows.Forms.ToolStripStatusLabel tooltipInfo;
        private System.Windows.Forms.ToolStripStatusLabel tooltipCnct;
        private System.Windows.Forms.Timer timer;
        private System.Windows.Forms.TableLayoutPanel tableBoard;
        private System.Windows.Forms.ImageList imageListGame;
        private System.Windows.Forms.ToolStripSeparator toolStripMenuItem2;
        private System.Windows.Forms.ToolStripMenuItem miGameTileOk;
        private System.Windows.Forms.ToolStripMenuItem miGameScores;
        private System.Windows.Forms.ToolStripStatusLabel tooltipColor;
        private System.Windows.Forms.ListBox lbLogs;
    }
}

