﻿using Newtonsoft.Json;
using Rever6Forms.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Rever6Forms
{
    public partial class FrmCreateJoin : Form
    {
        private string _server;

        public FrmCreateJoin(string server)
        {
            InitializeComponent();
            _server = server;
        }

        private void FrmCreateJoin_Load(object sender, EventArgs e)
        {
            btnRefresh_Click(btnRefresh, EventArgs.Empty);
        }

        private async void btnRefresh_Click(object sender, EventArgs e)
        {
            try
            {
                using (var http = new HttpClient())
                {
                    var response = await http.GetStringAsync($"{ _server }/games");
                    var dicGames = JsonConvert.DeserializeObject<IDictionary<string, OnlineGame>>(response);

                    lbGames.Items.Clear();
                    foreach (var gameKey in dicGames.Keys)
                    {
                        lbGames.Items.Add(gameKey);
                    }
                    if (lbGames.Items.Count > 0)
                    {
                        lbGames.SelectedIndex = 0;
                    }
                }
            }
            catch (HttpRequestException ex)
            {
                MessageBox.Show(this, ex.Message, "Récupération des parties", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            catch (JsonException ex)
            {
                MessageBox.Show(this, ex.Message, "Récupération des données", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            UpdateOkButton();
        }
        public string GameName => radioJoin.Checked
            ? lbGames.SelectedItem?.ToString()
            : txtNewGameName.Text;

        private void UpdateOkButton() => btnOk.Enabled = GameName != null && GameName != String.Empty;

        private void txtNewGameName_TextChanged(object sender, EventArgs e)
        {
            UpdateOkButton();
        }

        private void lbGames_SelectedIndexChanged(object sender, EventArgs e)
        {
            UpdateOkButton();
        }

        private void radioJoin_CheckedChanged(object sender, EventArgs e)
        {
            txtNewGameName.Enabled = radioNew.Checked;
            lbGames.Enabled = radioJoin.Checked;
            UpdateOkButton();
        }
    }
}
