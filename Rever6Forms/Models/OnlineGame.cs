﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Rever6Forms.Models
{
    public class OnlineGame
    {
        public int? Winner { get; set; }
        public int? CurPlayer { get; set; }

        public IEnumerable<int> Tiles;

        public IEnumerable<OnlinePlayLocation> CurPlayerPossibilities;
    }
}
