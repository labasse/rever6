﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Rever6Forms.Models
{
    public class OnlinePosition
    {
        public int Column { get; set; }
        public int Row { get; set; }
    }
}
