﻿namespace Rever6Forms.Models
{
    public class OnlinePlayLocation
    {
        public OnlinePosition Pos { get; set; }
        public string Score { get; set; }
    }
}