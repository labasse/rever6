﻿namespace Rever6Forms
{
    partial class FrmCreateJoin
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnOk = new System.Windows.Forms.Button();
            this.btnCancel = new System.Windows.Forms.Button();
            this.txtNewGameName = new System.Windows.Forms.TextBox();
            this.radioNew = new System.Windows.Forms.RadioButton();
            this.radioJoin = new System.Windows.Forms.RadioButton();
            this.btnRefresh = new System.Windows.Forms.Button();
            this.lbGames = new System.Windows.Forms.ListBox();
            this.SuspendLayout();
            // 
            // btnOk
            // 
            this.btnOk.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnOk.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.btnOk.Location = new System.Drawing.Point(126, 236);
            this.btnOk.Name = "btnOk";
            this.btnOk.Size = new System.Drawing.Size(75, 23);
            this.btnOk.TabIndex = 30;
            this.btnOk.Text = "OK";
            this.btnOk.UseVisualStyleBackColor = true;
            // 
            // btnCancel
            // 
            this.btnCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnCancel.Location = new System.Drawing.Point(220, 236);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(75, 23);
            this.btnCancel.TabIndex = 40;
            this.btnCancel.Text = "Annuler";
            this.btnCancel.UseVisualStyleBackColor = true;
            // 
            // txtNewGameName
            // 
            this.txtNewGameName.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtNewGameName.Location = new System.Drawing.Point(16, 30);
            this.txtNewGameName.Name = "txtNewGameName";
            this.txtNewGameName.Size = new System.Drawing.Size(279, 20);
            this.txtNewGameName.TabIndex = 10;
            this.txtNewGameName.Text = "Nouveau";
            this.txtNewGameName.TextChanged += new System.EventHandler(this.txtNewGameName_TextChanged);
            // 
            // radioNew
            // 
            this.radioNew.AutoSize = true;
            this.radioNew.Checked = true;
            this.radioNew.Location = new System.Drawing.Point(19, 7);
            this.radioNew.Name = "radioNew";
            this.radioNew.Size = new System.Drawing.Size(96, 17);
            this.radioNew.TabIndex = 41;
            this.radioNew.TabStop = true;
            this.radioNew.Text = "Nouvelle partie";
            this.radioNew.UseVisualStyleBackColor = true;
            // 
            // radioJoin
            // 
            this.radioJoin.AutoSize = true;
            this.radioJoin.Location = new System.Drawing.Point(19, 56);
            this.radioJoin.Name = "radioJoin";
            this.radioJoin.Size = new System.Drawing.Size(109, 17);
            this.radioJoin.TabIndex = 42;
            this.radioJoin.Text = "Joindre une partie";
            this.radioJoin.UseVisualStyleBackColor = true;
            this.radioJoin.CheckedChanged += new System.EventHandler(this.radioJoin_CheckedChanged);
            // 
            // btnRefresh
            // 
            this.btnRefresh.Location = new System.Drawing.Point(220, 56);
            this.btnRefresh.Name = "btnRefresh";
            this.btnRefresh.Size = new System.Drawing.Size(75, 23);
            this.btnRefresh.TabIndex = 43;
            this.btnRefresh.Text = "Actualiser";
            this.btnRefresh.UseVisualStyleBackColor = true;
            this.btnRefresh.Click += new System.EventHandler(this.btnRefresh_Click);
            // 
            // lbGames
            // 
            this.lbGames.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lbGames.Enabled = false;
            this.lbGames.FormattingEnabled = true;
            this.lbGames.Location = new System.Drawing.Point(15, 79);
            this.lbGames.Name = "lbGames";
            this.lbGames.Size = new System.Drawing.Size(280, 147);
            this.lbGames.TabIndex = 44;
            this.lbGames.SelectedIndexChanged += new System.EventHandler(this.lbGames_SelectedIndexChanged);
            // 
            // FrmCreateJoin
            // 
            this.AcceptButton = this.btnOk;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.btnCancel;
            this.ClientSize = new System.Drawing.Size(307, 271);
            this.Controls.Add(this.lbGames);
            this.Controls.Add(this.btnRefresh);
            this.Controls.Add(this.radioJoin);
            this.Controls.Add(this.radioNew);
            this.Controls.Add(this.txtNewGameName);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.btnOk);
            this.Name = "FrmCreateJoin";
            this.Text = "Créer/Joindre une partie";
            this.Load += new System.EventHandler(this.FrmCreateJoin_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnOk;
        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.TextBox txtNewGameName;
        private System.Windows.Forms.RadioButton radioNew;
        private System.Windows.Forms.RadioButton radioJoin;
        private System.Windows.Forms.Button btnRefresh;
        private System.Windows.Forms.ListBox lbGames;
    }
}