﻿using System;
using System.Collections.Generic;
using System.Linq;
using GangOf9.Common;
using GangOf9.Werew1;
using Moq;
using NUnit.Framework;

namespace TestGameOf9.Werew1
{
    public class TestWerew1
    {
        private const int
            FortuneTeller = 0,
            Werewolf1 = 1,
            Werewolf2 = 2,
            Villager1 = 3,
            Villager2 = 4,
            Villager3 = 5,
            Villager4 = 6,
            Villager5 = 7;
        private void InitFakeRandom()
        {
            var mockRnd = new Mock<IRandom>();

            mockRnd.SetupSequence(rnd => rnd.Next(8))
                .Returns(0).Returns(7)
                .Returns(1).Returns(6)
                .Returns(2).Returns(5)
                .Returns(3).Returns(4)
                .Returns(0);
            IRandom.SetFake(mockRnd.Object);
        }
        private (Game game, IEnumerable<IFullPlayer> players) NewGameAllPlayerJoined()
        {
            InitFakeRandom();
            var game = new Game();

            return (game, new IFullPlayer[]
            {
                game.Join("FT"),
                game.Join("W1"),
                game.Join("W2"),
                game.Join("V1"),
                game.Join("V2"),
                game.Join("V3"),
                game.Join("V4"),
                game.Join("V5")
            });
        }
        [Test]
        public void InitGame()
        {
            var test = new Game();

            Assert.AreEqual(GameTime.Lobby, test.Phase);
            Assert.AreEqual(8, test.WaitedPlayerNum);
            Assert.IsFalse (test.Players.Any());
        }
        [Test]
        public void CannotPlayInLobby()
        {
            var test = new Game();

            Assert.Throws<InvalidOperationException>(() => test.Play(new Turn(player: Werewolf1, voteFor: Villager1)));
        }
        [Test]
        public void Join1Player()
        {
            InitFakeRandom();
            var test = new Game();
            var actual = test.Join("A");

            Assert.AreEqual(test.Players.ElementAt(0), actual);
            Assert.AreEqual(0, actual.Index);
            Assert.AreEqual("A", actual.Name);
            Assert.IsTrue  (actual.IsAlive);
            Assert.AreEqual(GameRole.FortuneTeller, actual.Role);
            Assert.AreEqual(GameTime.Lobby, test.Phase);
            Assert.AreEqual(7, test.WaitedPlayerNum);
            Assert.AreEqual(1, test.Players.Count());
        }
        [Test]
        public void Join2Players()
        {
            InitFakeRandom();
            var test = new Game();
            test.Join("A");
            var actual = test.Join("B");

            Assert.AreEqual(test.Players.ElementAt(1), actual);
            Assert.AreEqual(1, actual.Index);
            Assert.AreEqual("B", actual.Name);
            Assert.IsTrue(actual.IsAlive);
            Assert.AreEqual(GameRole.Wereworlf, actual.Role);
            Assert.AreEqual(GameTime.Lobby, test.Phase);
            Assert.AreEqual(6, test.WaitedPlayerNum);
            Assert.AreEqual(2, test.Players.Count());
        }
        [Test]
        public void JoinAllPlayer()
        {
            (var game, var players) = NewGameAllPlayerJoined();

            Assert.AreEqual(GameTime.Night, game.Phase);
            Assert.AreEqual(2, game.WaitedPlayerNum);
            Assert.AreEqual(8, game.Players.Count());
            Assert.AreEqual(2, players.Count(p => p.Role == GameRole.Wereworlf));
            Assert.AreEqual(1, players.Count(p => p.Role == GameRole.FortuneTeller));
            Assert.AreEqual(5, players.Count(p => p.Role == GameRole.Villager));
            Assert.IsTrue(players.All(p => p.IsAlive && p.LastVillagerVote==null && p.Revealed==null));
        }
        [Test]
        public void CannotJoinInGame()
        {
            var test = NewGameAllPlayerJoined();

            Assert.Throws<InvalidOperationException>(() => test.game.Join("A"));
        }

        [Test]
        public void PlayInvalidPlayer()
        {
            (var test, var players) = NewGameAllPlayerJoined();

            Assert.Throws<ArgumentOutOfRangeException>(()=>test.Play(new Turn(player:8, voteFor:Villager1)));
        }
        [Test]
        public void PlayNegativePlayer()
        {
            (var test, var players) = NewGameAllPlayerJoined();

            Assert.Throws<ArgumentOutOfRangeException>(() => test.Play(new Turn(player: -1, voteFor: Villager1)));
        }
        [Test]
        public void PlayVillagerPlaysAtNight()
        {
            (var test, var players) = NewGameAllPlayerJoined();
            
            Assert.Throws<InvalidOperationException>(()=>test.Play(new Turn(player:Villager1, voteFor:Villager2)));
        }
        [Test]
        public void Play1WerewolfVoteForWereworl()
        {
            (var test, var players) = NewGameAllPlayerJoined();

            Assert.Throws<InvalidOperationException>(()=>test.Play(new Turn(player: Werewolf1, voteFor: Werewolf2)));
        }
        [Test]
        public void Play1WerewolfPlayed()
        {
            (var test, var players) = NewGameAllPlayerJoined();

            var actual = test.Play(new Turn(player: Werewolf1, voteFor: 0));

            Assert.IsNull(actual);
            Assert.AreEqual(GameTime.Night, test.Phase);
            Assert.AreEqual(1, test.WaitedPlayerNum);
        }
        [Test]
        public void Play2WerewolvesPlayed()
        {
            (var test, var players) = NewGameAllPlayerJoined();

            test.Play(new Turn(player: Werewolf2, voteFor: 0));
            var actual = test.Play(new Turn(player: Werewolf1, voteFor: 0));

            Assert.IsNull(actual);
            Assert.AreEqual(GameTime.Dawn, test.Phase);
            Assert.AreEqual(1, test.WaitedPlayerNum);
            Assert.IsTrue(test.Players.ElementAt(0).IsAlive);
        }
        [Test]
        public void Play2WerewolfVoteDifferent()
        {
            (var test, var players) = NewGameAllPlayerJoined();

            test.Play(new Turn(player: Werewolf2, voteFor: 0));
            var actual = test.Play(new Turn(player: Werewolf1, voteFor: 3));

            Assert.IsNull(actual);
            Assert.AreEqual(GameTime.Night, test.Phase);
            Assert.AreEqual(2, test.WaitedPlayerNum);
        }
        [Test, Sequential]
        public void PlayFortuneTellerReceiveRole(
            [Values(GameRole.Villager , GameRole.Wereworlf)] GameRole expected, 
            [Values(Villager1         , Werewolf1         )] int player)
        {
            (var test, var players) = NewGameAllPlayerJoined();

            test.Play(new Turn(player: Werewolf2, voteFor: 0));
            test.Play(new Turn(player: Werewolf1, voteFor: 0));

            Assert.AreEqual(expected, test.Play(new Turn(player:FortuneTeller, player)));
            Assert.AreEqual(GameTime.Day, test.Phase);
            Assert.AreEqual(7, test.WaitedPlayerNum);
            Assert.AreEqual(7, test.Players.Count(p => p.IsAlive));
            Assert.AreEqual(8, test.Players.Count());
            Assert.IsFalse(test.Players.ElementAt(0).IsAlive);
            Assert.AreEqual(GameRole.FortuneTeller, test.Players.ElementAt(0).Revealed);
        }
        [Test]
        public void PlayFortuneTellerVoteForDead()
        {
            (var test, var players) = NewGameAllPlayerJoined();

            test.Play(new Turn(player: Werewolf2, voteFor: Villager1));
            test.Play(new Turn(player: Werewolf1, voteFor: Villager1));
            test.Play(new Turn(player: FortuneTeller, voteFor: 2));

            test.Play(new Turn(player: Werewolf2, voteFor: Villager2));
            test.Play(new Turn(player: Werewolf1, voteFor: Villager2));
            test.Play(new Turn(player: FortuneTeller, voteFor: Werewolf1));
            test.Play(new Turn(player: Villager2, voteFor: Werewolf1));
            test.Play(new Turn(player: Villager3, voteFor: Werewolf1));
            test.Play(new Turn(player: Villager4, voteFor: Werewolf1));
            test.Play(new Turn(player: Villager5, voteFor: Villager3));

            test.Play(new Turn(player: Werewolf2, voteFor: Villager2));

            Assert.Throws<InvalidOperationException>(()=>test.Play(new Turn(player: FortuneTeller, voteFor: Werewolf1)));
        }
        [Test]
        public void PlayNonFortuneTellerPlayerPlaysAtDawn([Values(Villager1, Werewolf1)] int player)
        {
            (var test, var players) = NewGameAllPlayerJoined();

            test.Play(new Turn(player: Werewolf2, voteFor: FortuneTeller));
            test.Play(new Turn(player: Werewolf1, voteFor: FortuneTeller));

            Assert.Throws<InvalidOperationException>(() => test.Play(new Turn(player, 1)));
        }
        [Test]
        public void PlayDeadPlayerCannotPlay()
        {
            (var test, var players) = NewGameAllPlayerJoined();

            test.Play(new Turn(player: Werewolf2, voteFor: Villager1));
            test.Play(new Turn(player: Werewolf1, voteFor: Villager1));
            test.Play(new Turn(player: FortuneTeller, voteFor: 2));

            Assert.Throws<InvalidOperationException>(() => test.Play(new Turn(player: Villager1, voteFor: 2)));
        }
        [Test]
        public void Play1PlayerVote()
        {
            (var test, var players) = NewGameAllPlayerJoined();

            test.Play(new Turn(player: Werewolf2, voteFor: Villager1));
            test.Play(new Turn(player: Werewolf1, voteFor: Villager1));
            test.Play(new Turn(player: FortuneTeller, voteFor: 2));

            Assert.AreEqual(null, test.Play(new Turn(Villager2, 0)));
            Assert.AreEqual(GameTime.Day, test.Phase);
            Assert.AreEqual(0, test.Players.ElementAt(Villager2).LastVillagerVote);
            Assert.AreEqual(6, test.WaitedPlayerNum);
        }
        [Test]
        public void Play1PlayerVoteTwice()
        {
            (var test, var players) = NewGameAllPlayerJoined();

            test.Play(new Turn(player: Werewolf2, voteFor: Villager1));
            test.Play(new Turn(player: Werewolf1, voteFor: Villager1));
            test.Play(new Turn(player: FortuneTeller, voteFor: 2));
            test.Play(new Turn(Villager2, 0));

            Assert.Throws<InvalidOperationException>(() => test.Play(new Turn(Villager2, 0)));
        }
        [Test]
        public void PlayPlayerVoteDead()
        {
            (var test, var players) = NewGameAllPlayerJoined();

            test.Play(new Turn(player: Werewolf2, voteFor: Villager1));
            test.Play(new Turn(player: Werewolf1, voteFor: Villager1));
            test.Play(new Turn(player: FortuneTeller, voteFor: 2));
            
            Assert.Throws<InvalidOperationException>(() => test.Play(new Turn(Villager2, Villager1)));
        }
        [Test]
        public void PlayAllPlayerVoteForWereworlf()
        {
            (var test, var players) = NewGameAllPlayerJoined();

            test.Play(new Turn(player: Werewolf2, voteFor: Villager1));
            test.Play(new Turn(player: Werewolf1, voteFor: Villager1));
            test.Play(new Turn(player: FortuneTeller, voteFor: 2));

            test.Play(new Turn(player: Werewolf2    , voteFor: Villager2));
            test.Play(new Turn(player: Werewolf1    , voteFor: Villager2));
            test.Play(new Turn(player: FortuneTeller, voteFor: Werewolf1));
            test.Play(new Turn(player: Villager2    , voteFor: Werewolf1));
            test.Play(new Turn(player: Villager3    , voteFor: Werewolf1));
            test.Play(new Turn(player: Villager4    , voteFor: FortuneTeller));
            test.Play(new Turn(player: Villager5    , voteFor: Villager3));

            Assert.AreEqual(GameTime.Night, test.Phase);
            Assert.AreEqual(1, test.WaitedPlayerNum);
            Assert.AreEqual(6, test.Players.Count(p => p.IsAlive));
            Assert.AreEqual(8, test.Players.Count());
            Assert.IsFalse(test.Players.ElementAt(Werewolf1).IsAlive);
            Assert.AreEqual(GameRole.Wereworlf, test.Players.ElementAt(Werewolf1).Revealed);
        }
        [Test]
        public void PlayAllPlayerVoteForVillger()
        {
            (var test, var players) = NewGameAllPlayerJoined();

            test.Play(new Turn(player: Werewolf2, voteFor: Villager1));
            test.Play(new Turn(player: Werewolf1, voteFor: Villager1));
            test.Play(new Turn(player: FortuneTeller, voteFor: 2));

            test.Play(new Turn(player: Werewolf2, voteFor: Villager2));
            test.Play(new Turn(player: Werewolf1, voteFor: Villager2));
            test.Play(new Turn(player: FortuneTeller, voteFor: Werewolf1));
            test.Play(new Turn(player: Villager2, voteFor: Villager2));
            test.Play(new Turn(player: Villager3, voteFor: Werewolf1));
            test.Play(new Turn(player: Villager4, voteFor: FortuneTeller));
            test.Play(new Turn(player: Villager5, voteFor: Villager3));

            Assert.AreEqual(GameTime.Night, test.Phase);
            Assert.AreEqual(2, test.WaitedPlayerNum);
            Assert.AreEqual(6, test.Players.Count(p => p.IsAlive));
            Assert.AreEqual(8, test.Players.Count());
            Assert.IsFalse(test.Players.ElementAt(Villager2).IsAlive);
            Assert.AreEqual(GameRole.Villager, test.Players.ElementAt(Villager2).Revealed);
        }
        [Test]
        public void PlayAllPlayerWithDraw()
        {
            (var test, var players) = NewGameAllPlayerJoined();

            test.Play(new Turn(player: Werewolf2, voteFor: Villager1));
            test.Play(new Turn(player: Werewolf1, voteFor: Villager1));
            test.Play(new Turn(player: FortuneTeller, voteFor: 2));

            test.Play(new Turn(player: Werewolf2, voteFor: Villager2));
            test.Play(new Turn(player: Werewolf1, voteFor: Villager2));
            test.Play(new Turn(player: FortuneTeller, voteFor: Werewolf1));
            test.Play(new Turn(player: Villager2, voteFor: Werewolf1));
            test.Play(new Turn(player: Villager3, voteFor: Werewolf1));
            test.Play(new Turn(player: Villager4, voteFor: FortuneTeller));
            test.Play(new Turn(player: Villager5, voteFor: Villager2));

            Assert.AreEqual(GameTime.Day, test.Phase);
            Assert.AreEqual(7, test.WaitedPlayerNum);
            Assert.AreEqual(7, test.Players.Count(p => p.IsAlive));
        }
        [Test]
        public void PlayKilledFortuneTeller()
        {
            (var test, var players) = NewGameAllPlayerJoined();

            test.Play(new Turn(player: Werewolf2, voteFor: Villager1));
            test.Play(new Turn(player: Werewolf1, voteFor: Villager1));
            test.Play(new Turn(player: FortuneTeller, voteFor: 2));

            test.Play(new Turn(player: Werewolf2, voteFor: Villager2));
            test.Play(new Turn(player: Werewolf1, voteFor: Villager2));
            test.Play(new Turn(player: FortuneTeller, voteFor: Werewolf1));
            test.Play(new Turn(player: Villager2, voteFor: FortuneTeller));
            test.Play(new Turn(player: Villager3, voteFor: FortuneTeller));
            test.Play(new Turn(player: Villager4, voteFor: FortuneTeller));
            test.Play(new Turn(player: Villager5, voteFor: Villager3));

            test.Play(new Turn(player: Werewolf1, voteFor: Villager2));
            test.Play(new Turn(player: Werewolf2, voteFor: Villager2));

            Assert.AreEqual(GameTime.Day, test.Phase);
            Assert.AreEqual(5, test.WaitedPlayerNum);
        }
        [Test]
        public void PlayEndOfGameWerewolfWin()
        {
            (var test, var players) = NewGameAllPlayerJoined();

            test.Play(new Turn(player: Werewolf2, voteFor: Villager1));
            test.Play(new Turn(player: Werewolf1, voteFor: Villager1));
            test.Play(new Turn(player: FortuneTeller, voteFor: 2));

            test.Play(new Turn(player: Werewolf2, voteFor: Villager2));
            test.Play(new Turn(player: Werewolf1, voteFor: Villager2));
            test.Play(new Turn(player: FortuneTeller, voteFor: Werewolf1));
            test.Play(new Turn(player: Villager2, voteFor: FortuneTeller));
            test.Play(new Turn(player: Villager3, voteFor: FortuneTeller));
            test.Play(new Turn(player: Villager4, voteFor: FortuneTeller));
            test.Play(new Turn(player: Villager5, voteFor: Villager3));

            test.Play(new Turn(player: Werewolf1, voteFor: Villager2));
            test.Play(new Turn(player: Werewolf2, voteFor: Villager2));

            test.Play(new Turn(player: Werewolf2, voteFor: Villager3));
            test.Play(new Turn(player: Werewolf1, voteFor: Villager3));
            test.Play(new Turn(player: Villager3, voteFor: Werewolf1));
            test.Play(new Turn(player: Villager4, voteFor: Werewolf1));
            test.Play(new Turn(player: Villager5, voteFor: Werewolf1));

            test.Play(new Turn(player: Werewolf2, voteFor: Villager3));

            test.Play(new Turn(player: Werewolf2, voteFor: Villager4));
            test.Play(new Turn(player: Villager4, voteFor: Villager5));
            test.Play(new Turn(player: Villager5, voteFor: Villager4));

            test.Play(new Turn(player: Werewolf2, voteFor: Villager5));

            Assert.AreEqual(GameTime.WereworlvesWin, test.Phase);
            Assert.AreEqual(0, test.WaitedPlayerNum);
            Assert.IsTrue(test.Players.All(p => p.Revealed!=null));
        }
        [Test]
        public void EndOfGameVillagerWin()
        {
            (var test, var players) = NewGameAllPlayerJoined();

            test.Play(new Turn(player: Werewolf2, voteFor: Villager1));
            test.Play(new Turn(player: Werewolf1, voteFor: Villager1));
            test.Play(new Turn(player: FortuneTeller, voteFor: 2));

            test.Play(new Turn(player: Werewolf2, voteFor: Villager2));
            test.Play(new Turn(player: Werewolf1, voteFor: Villager2));
            test.Play(new Turn(player: FortuneTeller, voteFor: Werewolf1));
            test.Play(new Turn(player: Villager2, voteFor: FortuneTeller));
            test.Play(new Turn(player: Villager3, voteFor: FortuneTeller));
            test.Play(new Turn(player: Villager4, voteFor: FortuneTeller));
            test.Play(new Turn(player: Villager5, voteFor: Villager3));

            test.Play(new Turn(player: Werewolf1, voteFor: Villager2));
            test.Play(new Turn(player: Werewolf2, voteFor: Villager2));

            test.Play(new Turn(player: Werewolf2, voteFor: Villager3));
            test.Play(new Turn(player: Werewolf1, voteFor: Villager3));
            test.Play(new Turn(player: Villager3, voteFor: Werewolf1));
            test.Play(new Turn(player: Villager4, voteFor: Werewolf1));
            test.Play(new Turn(player: Villager5, voteFor: Werewolf1));

            test.Play(new Turn(player: Werewolf2, voteFor: Villager3));

            test.Play(new Turn(player: Werewolf2, voteFor: Villager4));
            test.Play(new Turn(player: Villager4, voteFor: Werewolf2));
            test.Play(new Turn(player: Villager5, voteFor: Werewolf2));

            Assert.AreEqual(GameTime.VillagersWin, test.Phase);
            Assert.AreEqual(0, test.WaitedPlayerNum);
            Assert.IsTrue(test.Players.All(p => p.Revealed != null));
        }
    }
}
