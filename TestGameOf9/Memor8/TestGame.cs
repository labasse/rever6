﻿using System;
using GangOf9.Memor8;
using NUnit.Framework;
using Moq;
using GangOf9.Common;

namespace TestGameOf9.Memor8
{
    public class TestGame
    {
        public const int X = Game.Hidden, _ = Game.Empty;

        private void InitFakeRandom()
        {
            var mockRnd = new Mock<IRandom>();

            mockRnd.SetupSequence(rnd => rnd.Next(3)).Returns(0).Returns(0);
            mockRnd.SetupSequence(rnd => rnd.Next(2)).Returns(1).Returns(0);
            IRandom.SetFake(mockRnd.Object);
        }


        [Test]
        public void InitGame()
        {
            var test = new Game(width: 3, height: 2, players: 4);

            Assert.AreEqual(4, test.PlayerCount);
            Assert.AreEqual(3, test.Width);
            Assert.AreEqual(2, test.Height);
            Assert.AreEqual(0, test.CurPlayer);
            Assert.IsNull(test.Winner);
            CollectionAssert.AreEqual(new int[] { 0, 0, 0, 0 }, test.Scores);
            CollectionAssert.AreEqual(new int[] {
                X, X, X,
                X, X, X
            }, test.Tiles);
        }
        [Test]
        public void InitMinimalGame()
        {
            var test = new Game(width: 2, height: 1, players: 1);

            Assert.AreEqual(1, test.PlayerCount);
            Assert.AreEqual(2, test.Width);
            Assert.AreEqual(1, test.Height);
            Assert.AreEqual(0, test.CurPlayer);
            Assert.IsNull(test.Winner);
            CollectionAssert.AreEqual(new int[] { 0 }, test.Scores);
            CollectionAssert.AreEqual(new int[] { X, X }, test.Tiles);
        }
        [Test]
        public void InitGameWithInvalidParams()
        {
            Assert.Throws<ArgumentOutOfRangeException>(() => new Game(width: 0, height: 3, players: 3));
        }
        [Test]
        public void InitGameWithInvalidPlayerNum()
        {
            Assert.Throws<ArgumentOutOfRangeException>(() => new Game(width: 0, height: 3, players: 0));
        }
        [Test]
        public void InitGameWithOddNumber()
        {
            Assert.Throws<ArgumentException>(() => new Game(width: 1, height: 3, players: 3));
        }
        [Test]
        public void PlayUnsuccessfullTry()
        {
            // 2 1 2
            // 1 3 3
            InitFakeRandom();
            var test = new Game(width: 3, height: 2, players: 4);
            var actual = test.Play(new Turn {
                Tile1 = new Rever6.Position(0, 0),
                Tile2 = new Rever6.Position(1, 0)
            });
            Assert.AreEqual(0, actual);
            Assert.AreEqual(1, test.CurPlayer);
            Assert.IsNull(test.Winner);
            CollectionAssert.AreEqual(new int[] { 0, 0, 0, 0 }, test.Scores);
            CollectionAssert.AreEqual(new int[] {
                2, 1, X,
                X, X, X
            }, test.Tiles);
        }
        [Test]
        public void PlaySuccessfullTry()
        {
            // 2 1 2
            // 1 3 3
            InitFakeRandom();
            var test = new Game(width: 3, height: 2, players: 3);
            var actual = test.Play(new Turn
            {
                Tile1 = new Rever6.Position(2, 0),
                Tile2 = new Rever6.Position(0, 0)
            });
            Assert.AreEqual(2, actual);
            Assert.AreEqual(0, test.CurPlayer);
            Assert.IsNull(test.Winner);
            CollectionAssert.AreEqual(new int[] { 1, 0, 0 }, test.Scores);
            CollectionAssert.AreEqual(new int[] {
                _, X, _,
                X, X, X
            }, test.Tiles);
        }
        [Test]
        public void PlayEmptyTile()
        {
            // 2 1 2
            // 1 3 3
            InitFakeRandom();
            var test = new Game(width: 3, height: 2, players: 3);

            test.Play(new Turn
            {
                Tile1 = new Rever6.Position(1, 0),
                Tile2 = new Rever6.Position(0, 1)
            });
            Assert.Throws<InvalidOperationException>(() => test.Play(new Turn
            {
                Tile1 = new Rever6.Position(0, 0),
                Tile2 = new Rever6.Position(0, 1)
            }));
        }
        [Test]
        public void PlayWith2ndSuccessfull()
        {
            // 2 1 2
            // 1 3 3
            InitFakeRandom();
            var test = new Game(width: 3, height: 2, players: 3);

            test.Play(new Turn
            {
                Tile1 = new Rever6.Position(0, 0),
                Tile2 = new Rever6.Position(1, 0)
            });
            var actual = test.Play(new Turn
            {
                Tile1 = new Rever6.Position(2, 0),
                Tile2 = new Rever6.Position(0, 0)
            });
            Assert.AreEqual(2, actual);
            Assert.AreEqual(1, test.CurPlayer);
            Assert.IsNull(test.Winner);
            CollectionAssert.AreEqual(new int[] { 0, 1, 0 }, test.Scores);
            CollectionAssert.AreEqual(new int[] {
                _, X, _,
                X, X, X
            }, test.Tiles);
        }
        [Test]
        public void Play3timesReturnsTo1stPlayer()
        {
            // 2 1 2
            // 1 3 3
            InitFakeRandom();
            var test = new Game(width: 3, height: 2, players: 3);

            test.Play(new Turn
            {
                Tile1 = new Rever6.Position(0, 0),
                Tile2 = new Rever6.Position(1, 0)
            });
            test.Play(new Turn
            {
                Tile1 = new Rever6.Position(0, 0),
                Tile2 = new Rever6.Position(1, 0)
            });
            var actual = test.Play(new Turn
            {
                Tile1 = new Rever6.Position(1, 0),
                Tile2 = new Rever6.Position(1, 1)
            });
            Assert.AreEqual(0, actual);
            Assert.AreEqual(0, test.CurPlayer);
            Assert.IsNull(test.Winner);
            CollectionAssert.AreEqual(new int[] { 0, 0, 0 }, test.Scores);
            CollectionAssert.AreEqual(new int[] {
                X, 1, X,
                X, 3, X
            }, test.Tiles);
        }
        [Test]
        public void PlayEndedGameWithNoWinner()
        {
           // 3 1 2 2
           // 1 3 4 4
           InitFakeRandom();
            var test = new Game(width: 4, height: 2, players: 2);

            test.Play(new Turn
            {
                Tile1 = new Rever6.Position(0, 0),
                Tile2 = new Rever6.Position(1, 1)
            });
            test.Play(new Turn
            {
                Tile1 = new Rever6.Position(1, 0),
                Tile2 = new Rever6.Position(0, 1)
            });
            test.Play(new Turn
            {
                Tile1 = new Rever6.Position(2, 0),
                Tile2 = new Rever6.Position(2, 1)
            });
            test.Play(new Turn
            {
                Tile1 = new Rever6.Position(2, 0),
                Tile2 = new Rever6.Position(3, 0)
            });
            var actual = test.Play(new Turn
            {
                Tile1 = new Rever6.Position(2, 1),
                Tile2 = new Rever6.Position(3, 1)
            });
            Assert.AreEqual(4, actual);
            Assert.IsNull(test.CurPlayer);
            Assert.IsNull(test.Winner);
            CollectionAssert.AreEqual(new int[] { 2, 2 }, test.Scores);
            CollectionAssert.AreEqual(new int[] {
                _, _, _, _,
                _, _, _, _
            }, test.Tiles); ;
        }
        [Test]
        public void PlayEndedGameWithWinner()
        {
            // 2 1 2
            // 1 3 3
            InitFakeRandom();
            var test = new Game(width: 3, height: 2, players: 3);

            test.Play(new Turn
            {
                Tile1 = new Rever6.Position(0, 0),
                Tile2 = new Rever6.Position(2, 0)
            });
            test.Play(new Turn
            {
                Tile1 = new Rever6.Position(1, 0),
                Tile2 = new Rever6.Position(1, 1)
            });
            test.Play(new Turn
            {
                Tile1 = new Rever6.Position(1, 0),
                Tile2 = new Rever6.Position(1, 1)
            });
            test.Play(new Turn
            {
                Tile1 = new Rever6.Position(1, 1),
                Tile2 = new Rever6.Position(2, 1)
            });
            var actual = test.Play(new Turn
            {
                Tile1 = new Rever6.Position(1, 0),
                Tile2 = new Rever6.Position(0, 1)
            });
            Assert.AreEqual(1, actual);
            Assert.IsNull(test.CurPlayer);
            Assert.AreEqual(2, test.Winner);
            CollectionAssert.AreEqual(new int[] { 1, 0, 2 }, test.Scores);
            CollectionAssert.AreEqual(new int[] {
                _, _, _,
                _, _, _
            }, test.Tiles);
        }

    }
}
