using System;
using GangOf9.TicTac3;
using NUnit.Framework;

namespace TestGameOf9.TicTac3
{
    public class Tests
    {
        private const int
            _ = Game._,
            X = Game.X,
            O = Game.O;

        public void PlayTile(Game test, int tileNum)
        {
            test.Play(new Turn() { TileNum = tileNum });
        }

        [Test]
        public void InitGame()
        {
            var test = new Game();

            Assert.AreEqual(Game.X, test.CurPlayer.Value);
            Assert.IsNull  (test.Winner);
            CollectionAssert.AreEqual(new int[] {
                _, _, _,
                _, _, _,
                _, _, _
            }, test.Tiles);
        }

        [Test]
        public void PlayLegalTurn()
        {
            var test = new Game();

            PlayTile(test, 1);
            Assert.AreEqual(Game.O, test.CurPlayer.Value);
            Assert.IsNull(test.Winner);
            CollectionAssert.AreEqual(new int[] {
                _, X, _,
                _, _, _,
                _, _, _
            }, test.Tiles);
        }
        [Test]
        public void PlayIllegalInvalidTileNum()
        {
            var test = new Game();

            Assert.Throws<ArgumentOutOfRangeException>(() => PlayTile(test, 9));
        }
        [Test]
        public void PlayAlreadyPlayedTileNum()
        {
            var test = new Game();

            PlayTile(test, 0);
            Assert.Throws<InvalidOperationException>(()=> PlayTile(test, 0));
        }
        [Test]
        public void PlayTwice()
        {
            var test = new Game();

            PlayTile(test, 0);
            PlayTile(test, 1);
            Assert.AreEqual(Game.X, test.CurPlayer.Value);
            Assert.IsNull(test.Winner);
            CollectionAssert.AreEqual(new int[] {
                X, O, _,
                _, _, _,
                _, _, _
            }, test.Tiles);
        }
        [Test]
        public void PlayDrawGame()
        {
            var test = new Game();

            //    X                   O
            PlayTile(test, 0); PlayTile(test, 1);
            PlayTile(test, 2); PlayTile(test, 3);
            PlayTile(test, 4); PlayTile(test, 6);
            PlayTile(test, 5); PlayTile(test, 8);
            PlayTile(test, 7);
            Assert.IsNull(test.CurPlayer);
            Assert.IsNull(test.Winner);
            CollectionAssert.AreEqual(new int[] {
                X, O, X,
                O, X, X,
                O, X, O
            }, test.Tiles);
        }
        [Test]
        public void PlayWiningGame()
        {
            var test = new Game();

            //    X                   O
            PlayTile(test, 0); PlayTile(test, 3);
            PlayTile(test, 1); PlayTile(test, 4);
            PlayTile(test, 8); PlayTile(test, 5);
            Assert.IsNull(test.CurPlayer);
            Assert.AreEqual(Game.O, test.Winner);
            CollectionAssert.AreEqual(new int[] {
                X, X, _,
                O, O, O,
                _, _, X
            }, test.Tiles);
        }
        [Test]
        public void PlayWiningGameOnLastTurn()
        {
            var test = new Game();

            PlayTile(test, 0); PlayTile(test, 1);
            PlayTile(test, 4); PlayTile(test, 2);
            PlayTile(test, 5); PlayTile(test, 3);
            PlayTile(test, 6); PlayTile(test, 7);
            PlayTile(test, 8);
            Assert.AreEqual(Game.X, test.Winner);
            Assert.IsNull(test.CurPlayer);
            CollectionAssert.AreEqual(new int[] {
                X, O, O,
                O, X, X,
                X, O, X
            }, test.Tiles);
        }
    }
}