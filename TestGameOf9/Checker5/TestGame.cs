﻿using System;
using GangOf9.Checker5;
using NUnit.Framework;

namespace TestGameOf9.Checker5
{
    class TestGame
    {
        private const int 
            _ = Game.Empty, 
            b = Game.Black, B = Game.BlackKing, 
            w = Game.White, W = Game.WhiteKing;

        private readonly int[] TypicalGame = {
              _ , W , _ , _ , _ , //  1- 5
            w , _ , _ , w , _ ,   //  6-10
              _ , w , w , _ , w , // 11-15
            _ , _ , b , b , w ,   // 16-20
              b , w , _ , _ , b , // 21-25
            w , _ , _ , b , _ ,   // 26-30
              _ , w , w , _ , _ , // 31-35
            b , _ , b , b , _ ,   // 36-40
              _ , b , _ , _ , b , // 41-45
            _ , _ , _ , B , _     // 46-50
        };

        // https://lidraughts.org/variant/standard

        [Test]
        public void InitGame()
        {
            var test = new Game();

            Assert.AreEqual (Game.White, test.CurPlayer);
            Assert.IsNull   (test.Winner);
            Assert.AreEqual (new int[] {
                  b , b , b , b , b , //  1- 5
                b , b , b , b , b ,   //  6-10
                  b , b , b , b , b , // 11-15
                b , b , b , b , b ,   // 16-20
                  _ , _ , _ , _ , _ , // 21-25
                _ , _ , _ , _ , _ ,   // 26-30
                  w , w , w , w , w , // 31-35
                w , w , w , w , w ,   // 36-40
                  w , w , w , w , w , // 41-45
                w , w , w , w , w ,   // 46-50
            }, test.Tiles);
        }
        [Test]
        public void InitWithStartedGame()
        {
            var test = new Game(TypicalGame, Game.Black);

            Assert.AreEqual(Game.Black, test.CurPlayer);
            Assert.IsNull(test.Winner);
            CollectionAssert.AreEqual(new int[] {
                  _ , W , _ , _ , _ , //  1- 5
                w , _ , _ , w , _ ,   //  6-10
                  _ , w , w , _ , w , // 11-15
                _ , _ , b , b , w ,   // 16-20
                  b , w , _ , _ , b , // 21-25
                w , _ , _ , b , _ ,   // 26-30
                  _ , w , w , _ , _ , // 31-35
                b , _ , b , b , _ ,   // 36-40
                  _ , b , _ , _ , b , // 41-45
                _ , _ , _ , B , _     // 46-50
            }, test.Tiles);
        }
        [Test]
        public void InitWithInvalidGame()
        {
            Assert.Throws<ArgumentException>(() => new Game(new int[] {
                  _ , _ , _ , _ , _ , //  1- 5
                _ , _ , _ , _ , _ ,   //  6-10
                  w , _ , _ , _ , _ , // 11-15
                _ , _ , _ , _ , _ ,   // 16-20
                  _ , _ , _ , _ , _ , // 21-25
                _ , _ , _ , _ , _ ,   // 26-30
                  _ , _ , _ , _ , _ , // 31-35
                _ , _ , _ , _ , b ,   // 36-40
                  _ , _ , _ , _ , _ , // 41-45
                _ , _ , _ , _ , _, _  // 46-50
            }, Game.Black));
        }
        [Test]
        public void InitWithEndedGame()
        {
            var test = new Game(new int[] {
                  _ , _ , _ , _ , _ , //  1- 5
                _ , _ , _ , _ , _ ,   //  6-10
                  w , _ , _ , _ , _ , // 11-15
                _ , _ , _ , _ , _ ,   // 16-20
                  _ , _ , _ , _ , _ , // 21-25
                _ , _ , _ , _ , _ ,   // 26-30
                  _ , _ , _ , _ , _ , // 31-35
                _ , _ , _ , _ , _ ,   // 36-40
                  _ , _ , _ , _ , _ , // 41-45
                _ , _ , _ , _ , _     // 46-50
            }, Game.Black);

            Assert.IsNull(test.CurPlayer);
            Assert.AreEqual(Game.White, test.Winner);
        }
        [Test]
        public void InitWithInvalidGameValues()
        {
            Assert.Throws<ArgumentOutOfRangeException>(()=> new Game(new int[] {
                  _ , _ , 5 , _ , _ , //  1- 5
                _ , _ , _ , _ , _ ,   //  6-10
                  w , _ , _ , _ , _ , // 11-15
                _ , _ , _ , _ , _ ,   // 16-20
                  _ , _ , _ , _ , _ , // 21-25
                _ , _ , _ , _ , _ ,   // 26-30
                  _ , _ , _ , _ , _ , // 31-35
                _ , _ , _ , _ , _ ,   // 36-40
                  _ , _ , _ , _ , _ , // 41-45
                _ , _ , _ , _ , _     // 46-50
            }, Game.Black));
        }
        [Test]
        public void PlayOnEndedGame()
        {
            var test = new Game(new int[] {
                  _ , _ , _ , _ , _ , //  1- 5
                _ , _ , _ , _ , _ ,   //  6-10
                  w , _ , _ , _ , _ , // 11-15
                _ , _ , _ , _ , _ ,   // 16-20
                  _ , _ , _ , _ , _ , // 21-25
                _ , _ , _ , _ , _ ,   // 26-30
                  _ , _ , _ , _ , _ , // 31-35
                _ , _ , _ , _ , _ ,   // 36-40
                  _ , _ , _ , _ , _ , // 41-45
                _ , _ , _ , _ , _     // 46-50
            }, Game.Black);
            Assert.Throws<InvalidOperationException>(() => test.Play(new Turn(11, 16)));
        }
        [Test]
        public void PlayWhiteWithBlackPawn()
        {
            var test = new Game(TypicalGame, Game.White);

            Assert.Throws<InvalidOperationException>(()=> test.Play(new Turn(29, 34)));
        }
        [Test]
        public void PlayWhiteMoveForwardLeft()
        {
            var test = new Game(TypicalGame, Game.White);
            test.Play(new Turn(12, 7));

            Assert.AreEqual(Game.Black, test.CurPlayer);
            Assert.IsNull(test.Winner);
            CollectionAssert.AreEqual(new int[] {
                  _ , W , _ , _ , _ , //  1- 5
                w , w , _ , w , _ ,   //  6-10
                  _ , _ , w , _ , w , // 11-15
                _ , _ , b , b , w ,   // 16-20
                  b , w , _ , _ , b , // 21-25
                w , _ , _ , b , _ ,   // 26-30
                  _ , w , w , _ , _ , // 31-35
                b , _ , b , b , _ ,   // 36-40
                  _ , b , _ , _ , b , // 41-45
                _ , _ , _ , B , _     // 46-50
            }, test.Tiles);
        }
        [Test]
        public void PlayWhiteMoveForwardRight()
        {
            var test = new Game(TypicalGame, Game.White);
            test.Play(new Turn(12, 8));

            Assert.AreEqual(Game.Black, test.CurPlayer);
            Assert.IsNull(test.Winner);
            CollectionAssert.AreEqual(new int[] {
                  _ , W , _ , _ , _ , //  1- 5
                w , _ , w , w , _ ,   //  6-10
                  _ , _ , w , _ , w , // 11-15
                _ , _ , b , b , w ,   // 16-20
                  b , w , _ , _ , b , // 21-25
                w , _ , _ , b , _ ,   // 26-30
                  _ , w , w , _ , _ , // 31-35
                b , _ , b , b , _ ,   // 36-40
                  _ , b , _ , _ , b , // 41-45
                _ , _ , _ , B , _     // 46-50
            }, test.Tiles);
        }
        [Test]
        public void PlayBlackMoveForwardRight()
        {
            var test = new Game(TypicalGame, Game.Black);
            test.Play(new Turn(39, 43));

            Assert.AreEqual(Game.White, test.CurPlayer);
            Assert.IsNull(test.Winner);
            CollectionAssert.AreEqual(new int[] {
                  _ , W , _ , _ , _ , //  1- 5
                w , _ , _ , w , _ ,   //  6-10
                  _ , w , w , _ , w , // 11-15
                _ , _ , b , b , w ,   // 16-20
                  b , w , _ , _ , b , // 21-25
                w , _ , _ , b , _ ,   // 26-30
                  _ , w , w , _ , _ , // 31-35
                b , _ , b , _ , _ ,   // 36-40
                  _ , b , b , _ , b , // 41-45
                _ , _ , _ , B , _     // 46-50
            }, test.Tiles);
        }
        [Test]
        public void PlayBlackMoveForwardLeft()
        {
            var test = new Game(TypicalGame, Game.Black);
            test.Play(new Turn(39, 44));

            Assert.AreEqual(Game.White, test.CurPlayer);
            Assert.IsNull(test.Winner);
            CollectionAssert.AreEqual(new int[] {
                  _ , W , _ , _ , _ , //  1- 5
                w , _ , _ , w , _ ,   //  6-10
                  _ , w , w , _ , w , // 11-15
                _ , _ , b , b , w ,   // 16-20
                  b , w , _ , _ , b , // 21-25
                w , _ , _ , b , _ ,   // 26-30
                  _ , w , w , _ , _ , // 31-35
                b , _ , b , _ , _ ,   // 36-40
                  _ , b , _ , b , b , // 41-45
                _ , _ , _ , B , _     // 46-50
            }, test.Tiles);
        }
        [Test]
        public void PlayWhiteMakeKing()
        {
            var test = new Game(TypicalGame, Game.White);
            test.Play(new Turn(9, 4));

            CollectionAssert.AreEqual(new int[] {
                  _ , W , _ , W , _ , //  1- 5
                w , _ , _ , _ , _ ,   //  6-10
                  _ , w , w , _ , w , // 11-15
                _ , _ , b , b , w ,   // 16-20
                  b , w , _ , _ , b , // 21-25
                w , _ , _ , b , _ ,   // 26-30
                  _ , w , w , _ , _ , // 31-35
                b , _ , b , b , _ ,   // 36-40
                  _ , b , _ , _ , b , // 41-45
                _ , _ , _ , B , _     // 46-50
            }, test.Tiles);
        }
        [Test]
        public void PlayBlackMakeKing()
        {
            var test = new Game(TypicalGame, Game.Black);
            test.Play(new Turn(42, 47));

            CollectionAssert.AreEqual(new int[] {
                  _ , W , _ , _ , _ , //  1- 5
                w , _ , _ , w , _ ,   //  6-10
                  _ , w , w , _ , w , // 11-15
                _ , _ , b , b , w ,   // 16-20
                  b , w , _ , _ , b , // 21-25
                w , _ , _ , b , _ ,   // 26-30
                  _ , w , w , _ , _ , // 31-35
                b , _ , b , b , _ ,   // 36-40
                  _ , _ , _ , _ , b , // 41-45
                _ , B , _ , B , _     // 46-50
            }, test.Tiles);
        }
        [Test]
        public void PlayWhiteImpossibleMoveForwardRight()
        {
            var test = new Game(TypicalGame, Game.White);

            Assert.Throws<InvalidOperationException>(() => test.Play(new Turn(15, 11)));
        }
        [Test]
        public void PlayWhiteImpossibleMoveForwardLeft()
        {
            var test = new Game(TypicalGame, Game.White);

            Assert.Throws<ArgumentOutOfRangeException>(() => test.Play(new Turn(6, 0)));
        }
        [Test]
        public void PlayWhiteImpossibleMoveForwardRightObstruction()
        {
            var test = new Game(TypicalGame, Game.White);

            Assert.Throws<InvalidOperationException>(()=> test.Play(new Turn(13, 9)));
        }
        [Test]
        public void PlayWhiteImpossibleMoveBackward()
        {
            var test = new Game(TypicalGame, Game.White);

            Assert.Throws<InvalidOperationException>(() => test.Play(new Turn(12, 17)));
        }
        [Test]
        public void PlayBlackImpossibleMoveForwardLeft()
        {
            var test = new Game(TypicalGame, Game.Black);

            Assert.Throws<InvalidOperationException>(() => test.Play(new Turn(36, 40)));
        }
        [Test]
        public void PlayBlackImpossibleMoveBackward()
        {
            var test = new Game(TypicalGame, Game.Black);

            Assert.Throws<InvalidOperationException>(() => test.Play(new Turn(39, 34)));
        }
        [Test]
        public void PlayBlackTakeForward()
        {
            var test = new Game(TypicalGame, Game.Black);

            test.Play(new Turn(18, 27));
            CollectionAssert.AreEqual(new int[] {
                  _ , W , _ , _ , _ , //  1- 5
                w , _ , _ , w , _ ,   //  6-10
                  _ , w , w , _ , w , // 11-15
                _ , _ , _ , b , w ,   // 16-20
                  b , _ , _ , _ , b , // 21-25
                w , b , _ , b , _ ,   // 26-30
                  _ , w , w , _ , _ , // 31-35
                b , _ , b , b , _ ,   // 36-40
                  _ , b , _ , _ , b , // 41-45
                _ , _ , _ , B , _     // 46-50
            }, test.Tiles);
        }
        [Test]
        public void PlayBlackTakeBackward()
        {
            var test = new Game(TypicalGame, Game.Black);

            test.Play(new Turn(38, 27));
            CollectionAssert.AreEqual(new int[] {
                  _ , W , _ , _ , _ , //  1- 5
                w , _ , _ , w , _ ,   //  6-10
                  _ , w , w , _ , w , // 11-15
                _ , _ , b , b , w ,   // 16-20
                  b , w , _ , _ , b , // 21-25
                w , b , _ , b , _ ,   // 26-30
                  _ , _ , w , _ , _ , // 31-35
                b , _ , _ , b , _ ,   // 36-40
                  _ , b , _ , _ , b , // 41-45
                _ , _ , _ , B , _     // 46-50
            }, test.Tiles);
        }
        [Test]
        public void PlayWhiteTakeForward()
        {
            var test = new Game(TypicalGame, Game.White);

            test.Play(new Turn(33, 24));
            CollectionAssert.AreEqual(new int[] {
                  _ , W , _ , _ , _ , //  1- 5
                w , _ , _ , w , _ ,   //  6-10
                  _ , w , w , _ , w , // 11-15
                _ , _ , b , b , w ,   // 16-20
                  b , w , _ , w , b , // 21-25
                w , _ , _ , _ , _ ,   // 26-30
                  _ , w , _ , _ , _ , // 31-35
                b , _ , b , b , _ ,   // 36-40
                  _ , b , _ , _ , b , // 41-45
                _ , _ , _ , B , _     // 46-50
            }, test.Tiles);
        }
        [Test]
        public void PlayWhiteTakeBackward()
        {
            var test = new Game(TypicalGame, Game.White);

            test.Play(new Turn(13, 24));
            CollectionAssert.AreEqual(new int[] {
                  _ , W , _ , _ , _ , //  1- 5
                w , _ , _ , w , _ ,   //  6-10
                  _ , w , _ , _ , w , // 11-15
                _ , _ , b , _ , w ,   // 16-20
                  b , w , _ , w , b , // 21-25
                w , _ , _ , b , _ ,   // 26-30
                  _ , w , w , _ , _ , // 31-35
                b , _ , b , b , _ ,   // 36-40
                  _ , b , _ , _ , b , // 41-45
                _ , _ , _ , B , _     // 46-50
            }, test.Tiles);
        }
        [Test]
        public void PlayBlackTakeLast()
        {
            var test = new Game(new int[] {
                  _ , _ , _ , _ , _ , //  1- 5
                _ , _ , _ , _ , _ ,   //  6-10
                  _ , _ , _ , _ , _ , // 11-15
                _ , _ , _ , _ , _ ,   // 16-20
                  _ , _ , _ , _ , _ , // 21-25
                _ , _ , _ , _ , _ ,   // 26-30
                  _ , _ , _ , w , _ , // 31-35
                _ , _ , _ , _ , b ,   // 36-40
                  _ , _ , _ , _ , _ , // 41-45
                _ , _ , _ , _ , _,    // 46-50
            }, Game.Black);
            test.Play(new Turn(40, 29));
            Assert.AreEqual(Game.Black, test.Winner);
            Assert.IsNull(test.CurPlayer);
        }
    }
}
