﻿using System;
using GangOf9.Shi4me;
using NUnit.Framework;

namespace TestGameOf9.Shi4Me
{
    public class TestGame
    {
        [Test]
        public void InitGame() 
        {
            var test = new Game();

            Assert.AreEqual(0, test.TurnNum);
            Assert.AreEqual(2, test.WaitedPlayerNum);
            Assert.AreEqual(0, test.Score1);
            Assert.AreEqual(0, test.Score2);
            Assert.IsNull(test.Winner);
        }
        [Test]
        public void PlayInvalidPlayer()
        {
            var test = new Game();

            Assert.Throws<ArgumentOutOfRangeException>(() => test.Play(new Turn() { Player = 0, Choice = Objects.Rock }));
        }
        [Test]
        public void Play1player()
        {
            var test = new Game();

            test.Play(new Turn() { Player = 1, Choice = Objects.Rock });
            Assert.AreEqual(0, test.TurnNum);
            Assert.AreEqual(1, test.WaitedPlayerNum);
            Assert.AreEqual(0, test.Score1);
            Assert.AreEqual(0, test.Score2);
            Assert.IsNull(test.Winner);
        }
        [Test]
        public void PlaySamePlayerPlaysTwice()
        {
            var test = new Game();

            test.Play(new Turn() { Player = 1, Choice = Objects.Rock });
            Assert.Throws<InvalidOperationException>(() => test.Play(new Turn() { Player = 1, Choice = Objects.Paper }));
        }
        [Test]
        public void Play1TimeAnd1Player()
        {
            var test = new Game();

            test.Play(new Turn() { Player = 1, Choice = Objects.Rock });
            test.Play(new Turn() { Player = 2, Choice = Objects.Scissors });

            test.Play(new Turn() { Player = 1, Choice = Objects.Rock });
            Assert.AreEqual(1, test.TurnNum);
            Assert.AreEqual(1, test.WaitedPlayerNum);
            Assert.AreEqual(1, test.Score1);
            Assert.AreEqual(0, test.Score2);
            Assert.IsNull(test.Winner);
        }
        [Test]
        public void PlayMultipleTimes()
        {
            var test = new Game();

            test.Play(new Turn() { Player = 1, Choice = Objects.Rock });
            test.Play(new Turn() { Player = 2, Choice = Objects.Scissors });
            
            test.Play(new Turn() { Player = 1, Choice = Objects.Rock });
            test.Play(new Turn() { Player = 2, Choice = Objects.Paper });
            
            test.Play(new Turn() { Player = 1, Choice = Objects.Paper });
            test.Play(new Turn() { Player = 2, Choice = Objects.Rock });
            Assert.AreEqual(3, test.TurnNum);
            Assert.AreEqual(2, test.WaitedPlayerNum);
            Assert.AreEqual(2, test.Score1);
            Assert.AreEqual(1, test.Score2);
            Assert.AreEqual(1, test.Winner);
        }
        [Test, Sequential]
        public void PlayPlayer1Wins(
            [Values(Objects.Rock, Objects.Paper, Objects.Scissors)] Objects player1,
            [Values(Objects.Scissors, Objects.Rock, Objects.Paper)] Objects player2)
        {
            var test = new Game();

            test.Play(new Turn() { Player = 1, Choice = player1 });
            test.Play(new Turn() { Player = 2, Choice = player2 });
            Assert.AreEqual(1, test.TurnNum);
            Assert.AreEqual(1, test.Score1);
            Assert.AreEqual(0, test.Score2);
            Assert.AreEqual(1, test.Winner);
        }
        [Test, Sequential]
        public void PlayPlayer2Wins(
            [Values(Objects.Scissors, Objects.Rock , Objects.Paper   )] Objects player1,
            [Values(Objects.Rock    , Objects.Paper, Objects.Scissors)] Objects player2
        )
        {
            var test = new Game();

            test.Play(new Turn() { Player = 1, Choice = player1 });
            test.Play(new Turn() { Player = 2, Choice = player2 });
            Assert.AreEqual(1, test.TurnNum);
            Assert.AreEqual(0, test.Score1);
            Assert.AreEqual(1, test.Score2);
            Assert.AreEqual(2, test.Winner);
        }
        [Test]
        public void PlayWithDrawResult([Values(Objects.Rock, Objects.Paper, Objects.Scissors)] Objects choice)
        {
            var test = new Game();

            test.Play(new Turn() { Player = 2, Choice = choice });
            test.Play(new Turn() { Player = 1, Choice = choice });
            Assert.AreEqual(1, test.TurnNum);
            Assert.AreEqual(0, test.Score1);
            Assert.AreEqual(0, test.Score2);
            Assert.IsNull(test.Winner);
        }

    }
}
