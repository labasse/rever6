﻿using Rever6;
using System;

namespace Rever6Client
{
    class Program
    {
        private static void DrawLine()
        {
            Console.Write("   ");
            for (var i = 0; i < Game.Size; i++)
            {
                Console.Write("+---");
            }
            Console.WriteLine("+");
        }
        static void Main(string[] args)
        {
            var game = new Game();

            while(game.CurPlayer!=null)
            {
                Console.Write("   ");
                for (int col = 0; col < Game.Size; col++)
                {
                    Console.Write($"  { Position.GetColumnName(col) } ");
                }
                Console.WriteLine("");
                for (int row = 0; row < Game.Size; row++)
                {
                    DrawLine();
                    Console.Write($" { Position.GetRowName(row) } ");
                    for (int col = 0; col < Game.Size; col++)
                    {
                        var tile = '?';

                        switch (game.GetTile(col, row))
                        {
                            case Game.Empty: tile = ' '; break;
                            case Game.Black: tile = 'X'; break;
                            case Game.White: tile = 'O'; break;
                        }
                        Console.Write($"| { tile } ");
                    }
                    Console.WriteLine("|");
                }
                DrawLine();

                try
                {
                    Console.WriteLine("Quel case ?");
                    var choice = Position.Parse(Console.ReadLine());

                    var result = game.Play(choice);

                    Console.WriteLine($"{result} pion-s retourné-s");
                }
                catch (ArgumentException e)
                {
                    Console.Error.WriteLine($"Case non valide : {e.Message}");
                }
                catch (InvalidOperationException e)
                {
                    Console.Error.WriteLine($"Impossible de jouer ici ({e.Message})");
                }
            }
        }
    }
}
