﻿using System;
using System.Collections.Generic;
using System.Text;

namespace GangOf9.TicTac3
{
    public class Game
    {
        public const int
            Empty = 0,
            _ = 0,
            X = -1,
            O = 1;
        private int turns = 0;
        private int[] _board = new int[9];

        public int? CurPlayer { get; private set; } = Game.X;
        public int? Winner { get; private set; }

        public IEnumerable<int> Tiles => _board;

        public void Play(Turn t)
        {
            if(t.TileNum < 0 || _board.Length <= t.TileNum)
            {
                throw new ArgumentOutOfRangeException("TileNum is out of bounds");
            }
            if(_board[t.TileNum]!=0)
            {
                throw new InvalidOperationException("Already played here");
            }
            _board[t.TileNum] = CurPlayer.Value;
            if (!CheckWin(start:4, inc:4) && !CheckWin(start:4, inc:2))
            {
                for (int i = 0; i < 3; i++)
                {
                    if (CheckWin(start: 1+i*3, inc: 1) || CheckWin(start: 3+i, inc: 3))
                    {
                        return;
                    }
                }
                CurPlayer = ++turns == _board.Length ? null : - CurPlayer;
            }            
        }
        private bool CheckWin(int start, int inc)
        {
            if(_board[start]!=0 && _board[start-inc] == _board[start] && _board[start]== _board[start+inc])
            {
                CurPlayer = null;
                Winner = _board[start];
                return true;
            }
            return false;
        }
    }
}
