﻿using System;
using System.Collections.Generic;
using System.Text;

namespace GangOf9.Common
{
    public interface IRandom
    {
        int Next(int max);

        public static IRandom Current { get; private set; } = new TrueRandom();

        public static void SetFake(IRandom fake)
        {
            Current = fake;
        }

        private class TrueRandom : IRandom
        {
            private Random rnd = new Random();

            public int Next(int max) => rnd.Next(max);
        }
    }
}
