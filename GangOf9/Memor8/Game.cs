﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GangOf9.Common;

namespace GangOf9.Memor8
{
    public class Game
    {
        public const int Empty = -1;
        public const int Hidden = 0;
        private int[] _scores;
        private int[,] _board;
        private Turn _last;
        
        public Game(int width, int height, int players)
        {
            if(players<1)
            {
                throw new ArgumentOutOfRangeException("Player count must be greater than 0");
            }
            if(width<1 || height<1)
            {
                throw new ArgumentOutOfRangeException("Width and height must be greater than 0");
            }
            if((width*height)%2 != 0)
            {
                throw new ArgumentException("Number of tile must be event, not odd");
            }
            Width = width;
            Height = height;
            PlayerCount = players;
            _scores = new int[players];
            _board = new int[width, height];
            for (var row = 0; row < Height; row++)
            {
                for (var col = 0; col < Width; col++)
                {
                    _board[col, row] = 1 + (col + Width * row) / 2;
                }
            }
            for(int i=0; i<Width*Height; i++)
            {
                var col1 = IRandom.Current.Next(Width);
                var row1 = IRandom.Current.Next(Height);
                var col2 = IRandom.Current.Next(Width);
                var row2 = IRandom.Current.Next(Height);
                var tile1 = _board[col1, row1];

                _board[col1, row1] = _board[col2, row2];
                _board[col2, row2] = tile1;
            }
        }
        public int Width { get; private set;  }
        public int Height { get; private set; }
        public int PlayerCount { get; private set; }

        public int? CurPlayer { get; private set; } = 0;
        public int? Winner { get; private set; }

        public IEnumerable<int> Scores => _scores;

        private bool PosEqual(Rever6.Position pos, int col, int row) =>
            pos.Column == col && pos.Row == row;

        public int GetTile(int col, int row)
        {
            if( _board[col, row] == Empty || 
                _last != null && (PosEqual(_last.Tile1, col, row) || PosEqual(_last.Tile2, col, row)))
            {
                return _board[col, row];
            }
            else
            {
                return Hidden;
            }
        }

        public IEnumerable<int> Tiles
        {
            get
            {
                for (var row = 0; row < Height; row++)
                {
                    for (var col = 0; col < Width; col++)
                    {
                        yield return GetTile(col, row);
                    }
                }
            }
        }

        private int GetTile(Rever6.Position pos) => _board[pos.Column, pos.Row];
        private void SetTile(Rever6.Position pos, int val) => _board[pos.Column, pos.Row] = val;

        public int Play(Turn t)
        {
            var tile1 = GetTile(t.Tile1);
            var tile2 = GetTile(t.Tile2);

            if (tile1==Empty || tile2==Empty)
            {
                throw new InvalidOperationException("Cannot play on an empty tile");
            }
            if(tile1==tile2)
            {
                SetTile(t.Tile1, Empty);
                SetTile(t.Tile2, Empty);
                _last = null;
                _scores[CurPlayer.Value]++;
                if(_scores.Sum()*2==Width*Height)
                {
                    Winner = 0;
                    CurPlayer = null;
                    for(var i = 1; i<_scores.Length; i++)
                    {
                        if(_scores[i]>_scores[Winner.Value])
                        {
                            Winner = i;
                        }
                        else if(_scores[i] == _scores[Winner.Value])
                        {
                            Winner = null;
                            break;
                        }
                    }
                }
                return tile1;
            }
            else
            {
                if(++CurPlayer == PlayerCount)
                {
                    CurPlayer = 0;
                }
                _last = new Turn() { Tile1 = t.Tile1, Tile2 = t.Tile2 };
                return 0;
            }
        }
    }
}
