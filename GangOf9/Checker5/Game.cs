﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GangOf9.Checker5
{
    public class Game
    {
        public const int Size = 10;
        public const int LastTile = Size * Size / 2;
        public const int 
            Empty =  0,
            Black =  1, BlackKing = Black * 2,
            White = -1, WhiteKing = White * 2;

        private int[] _board = {
               1 , 1 , 1 , 1 , 1 ,
             1 , 1 , 1 , 1 , 1 ,
               1 , 1 , 1 , 1 , 1 ,
             1 , 1 , 1 , 1 , 1 ,
               0 , 0 , 0 , 0 , 0 ,
             0 , 0 , 0 , 0 , 0 ,
              -1 ,-1 ,-1 ,-1 ,-1 ,
            -1 ,-1 ,-1 ,-1 ,-1 ,
              -1 ,-1 ,-1 ,-1 ,-1 ,
            -1 ,-1 ,-1 ,-1 ,-1
        };
        public Game()
        {

        }
        public Game(IEnumerable<int> init, int color)
        {
            if(color!=Game.Black && color!=Game.White)
            {
                throw new ArgumentException("Color must be -1 or 1");
            }
            if(init.Count()!=Size*Size/2)
            {
                throw new ArgumentException("Invalid initialisation board");
            }
            if(init.Any(val => val<Game.WhiteKing || Game.BlackKing<val))
            {
                throw new ArgumentOutOfRangeException("Invalid value in initialization board");
            }
            _board = init.ToArray();
            NextPlayer(color);
        }
        private void NextPlayer(int nextPlayer)
        {
            var min = Empty;
            var max = Empty;
            foreach(var val in _board)
            {
                if(val<min)
                {
                    min = val;
                }
                else if(val>max)
                {
                    max = val;
                }
            }
            if(min==0 || max==0)
            {
                CurPlayer = null;
                Winner = min==0 ? Game.Black : Game.White;
            }
            else
            {
                CurPlayer = nextPlayer;
            }
        }
        public int? CurPlayer { get; private set; } = Game.White;
        public int? Winner { get; private set; }
        public IEnumerable<int> Tiles => _board; 
        public void Play(Turn t)
        {
            if(t.BeginPos<1 || LastTile<t.BeginPos || t.EndPos < 1 || LastTile < t.EndPos)
            {
                throw new ArgumentOutOfRangeException("Turn position must be in 1..50");
            }
            if(CurPlayer==null)
            {
                throw new InvalidOperationException("Cannot play on an ended game");
            }
            var begin = t.BeginPos - 1;
            var end = t.EndPos - 1;
            if(_board[begin]!=CurPlayer)
            {
                throw new InvalidOperationException("Cannot play with bad color");
            }
            if(_board[end]!=Empty)
            {
                throw new InvalidOperationException("Cannot go to an occupied tile");
            }
            var opponent = -CurPlayer.Value;
            
            _board[end] = _board[begin];
            _board[begin] = Empty;
            
            if((end/5==0 && CurPlayer==White || end/5==9 && CurPlayer==Black) && Math.Abs(_board[end]) == 1)
            {
                _board[end] *= 2;
            }
            NextPlayer(opponent);
        }
    //     00  01  02  03  04 
    //   05  06  07  08  09
    //     10  11  12  13  14
    //   15  16  17  18  19
    //     20  21  22  23  24 
    //   25  26  27  28  29
    //     30  31  32  33  34
    //   35  36  37  38  39
    }
}
