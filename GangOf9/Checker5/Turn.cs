﻿using System;
using System.Collections.Generic;
using System.Text;

namespace GangOf9.Checker5
{
    public class Turn
    {
        public Turn(int beginPos, int endPos)
        {
            BeginPos = beginPos;
            EndPos = endPos;
        }
        public int BeginPos { get; set; }
        public int EndPos { get; set; }
    }
}
