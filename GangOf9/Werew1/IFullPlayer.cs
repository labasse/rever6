﻿using System;
using System.Collections.Generic;
using System.Text;

namespace GangOf9.Werew1
{
    public interface IFullPlayer : IPublicPlayer
    {
        GameRole Role { get; }
    }
}
