﻿namespace GangOf9.Werew1
{
    public class Turn
    {
        public Turn(int player, int voteFor)
        {
            Player = player;
            VoteFor = voteFor;
        }
        public int Player { get; set; }
        public int VoteFor { get; set; }
    }
}
