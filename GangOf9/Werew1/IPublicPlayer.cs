﻿using System;
using System.Collections.Generic;
using System.Text;

namespace GangOf9.Werew1
{
    public interface IPublicPlayer
    {
        int Index { get; }
        string Name { get; }
        bool IsAlive { get; }
        GameRole? Revealed { get; }
        int? LastVillagerVote { get; }
    }
}
