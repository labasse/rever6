﻿using System;
using System.Collections.Generic;
using System.Text;

namespace GangOf9.Werew1.States
{
    abstract class State
    {
        protected GameContext Context { get; }
        public GameTime Phase { get; }

        public State(GameTime phase, GameContext context)
        {
            Context = context;
            Phase = phase;
        }
        public virtual void Enter() { }
        public abstract IFullPlayer Join(string name);
    
        public abstract int WaitedPlayerNum { get; }

        public abstract GameRole? Play(Turn t);

        protected class Player : IFullPlayer
        {
            public Player(int index, string name, GameRole role)
            {
                Name = name;
                Role = role;
                Index = index;
            }

            public void Kill()
            {
                IsAlive = false;
                Reveal();
            }
            public void Reveal()
            {
                Revealed = Role;
            }

            public string Name { get; }

            public bool IsAlive { get; private set; } = true;

            public GameRole Role { get; }

            public int Index { get; }

            public GameRole? Revealed { get; private set; }

            public int? LastVillagerVote { get; set; }
        }
    }
}
