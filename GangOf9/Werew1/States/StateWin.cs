﻿using System;

namespace GangOf9.Werew1.States
{
    class StateWin : State
    {
        public StateWin(GameTime phase, GameContext context) : base(phase, context)
        {

        }
        public override int WaitedPlayerNum => 0;

        public override IFullPlayer Join(string name) => throw new InvalidOperationException("Cannot join an ended game");
        
        public override GameRole? Play(Turn t) => throw new InvalidOperationException("Cannot play on an ended game");
    }
}
