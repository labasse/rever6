﻿using System.Collections.Generic;

namespace GangOf9.Werew1.States
{
    class GameContext
    {
        public const int 
            PlayerCount = 8,
            WerewolfCount = 2,
            VillagersCount = 5;
        
        private IDictionary<GameTime, State> _states = new Dictionary<GameTime, State>();

        public void Register(State newState)
        {
            _states[newState.Phase] = newState;
            if(CurState==null)
            {
                ChangeState(newState.Phase);
            }
        }
        public void ChangeState(GameTime state)
        {
            CurState = _states[state];
            CurState.Enter();
        }

        public IDictionary<string, object> Bag { get; private set; } = new Dictionary<string, object>();

        public State CurState { get; private set; }

        public List<IFullPlayer> Players { get; private set; } = new List<IFullPlayer>();
    }
}
