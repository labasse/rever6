﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GangOf9.Werew1.States
{
    class StateDay : StateWithVote
    {
        public StateDay(GameContext context) : base(GameTime.Day, context, voter=>voter.IsAlive)
        {

        }

        public override void Enter()
        {
            var victim = Context.Bag[StateNight.WerewolfVictim];

            if (victim != null)
            {
                var phase = Kill((int)victim, GameTime.Day);

                if (phase != GameTime.Day)
                {
                    Context.ChangeState(phase);
                    return;
                }
                Context.Bag[StateNight.WerewolfVictim] = null;
            }
            base.Enter();
        }

        protected override void OnVote(Player player, Player voteFor) => player.LastVillagerVote = voteFor.Index;
        
        protected override GameTime? ValidateVotes(int electedPlayer, int voteCount1, int voteCount2) =>
            voteCount1 == voteCount2
                ? (GameTime?)null
                : Kill(electedPlayer, GameTime.Night);
        
        private GameTime Reveal(IEnumerable<IFullPlayer> players, GameTime nextPhase)
        {
            foreach(Player p in players)
            {
                p.Reveal();
            }
            return nextPhase;
        }
        private GameTime Kill(int player, GameTime next)
        {
            ((Player)Context.Players[player]).Kill();
            var surrenders = Context.Players.Where(p => p.IsAlive);

            if (surrenders.All(p=>p.Role==GameRole.Wereworlf))
            {
                return Reveal(surrenders, GameTime.WereworlvesWin);
            }
            else if(surrenders.All(p=>p.Role!=GameRole.Wereworlf))
            {
                return Reveal(surrenders, GameTime.VillagersWin);
            }
            else
            {
                return next;
            }
        }
    }
}

