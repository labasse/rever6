﻿using GangOf9.Common;
using System;

namespace GangOf9.Werew1.States
{
    class StateLobby : State
    {
        private GameRole[] _playerRoles = new GameRole[GameContext.PlayerCount];

        public StateLobby(GameContext context) : base(GameTime.Lobby, context)
        {
            
        }
        public override void Enter()
        {
            for (var i = 0; i < GameContext.VillagersCount; i++)
            {
                _playerRoles[i] = GameRole.Villager;
            }
            for (var i = 0; i < GameContext.WerewolfCount; i++)
            {
                _playerRoles[GameContext.VillagersCount + i] = GameRole.Wereworlf;
            }
            _playerRoles[GameContext.VillagersCount+GameContext.WerewolfCount] = GameRole.FortuneTeller;
            for (var i = 0; i < GameContext.PlayerCount; i++)
            {
                var role1 = IRandom.Current.Next(_playerRoles.Length);
                var role2 = IRandom.Current.Next(_playerRoles.Length);
                var tmp = _playerRoles[role1];

                _playerRoles[role1] = _playerRoles[role2];
                _playerRoles[role2] = tmp;
            }
        }
        public override int WaitedPlayerNum => GameContext.PlayerCount - Context.Players.Count;

        public override IFullPlayer Join(string name)
        {
            var index = Context.Players.Count;
            var newPlayer = new Player(index, name, _playerRoles[index]);
            
            Context.Players.Add(newPlayer);
            if(WaitedPlayerNum == 0)
            {
                Context.ChangeState(GameTime.Night);
            }
            return newPlayer;
        }

        public override GameRole? Play(Turn t) => throw new InvalidOperationException("Cannot play in lobby");
    }
}
