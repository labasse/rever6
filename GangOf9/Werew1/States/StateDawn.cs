﻿using System;
using System.Linq;

namespace GangOf9.Werew1.States
{
    class StateDawn : State
    {
        public StateDawn(GameContext context):base(GameTime.Dawn, context)
        {

        }
        public override void Enter()
        {
            if(Context.Players.Count(p => p.IsAlive && p.Role==GameRole.FortuneTeller)==0)
            {
                Context.ChangeState(GameTime.Day);
            }
        }
        public override int WaitedPlayerNum => 1;

        public override IFullPlayer Join(string name) => throw new InvalidOperationException("Cannot join a started game"); 
        
        public override GameRole? Play(Turn t)
        {
            var player = Context.Players[t.Player];
            var voteFor = Context.Players[t.VoteFor];

            if(player.Role!=GameRole.FortuneTeller)
            {
                throw new InvalidOperationException("Only Fortune teller can play at dawn");
            }
            if(!voteFor.IsAlive)
            {
                throw new InvalidOperationException("Cannot ask to see dead player");
            }
            Context.ChangeState(GameTime.Day);
            return voteFor.Role;
        }
    }
}
