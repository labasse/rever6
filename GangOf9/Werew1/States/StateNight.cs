﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GangOf9.Werew1.States
{
    class StateNight : StateWithVote
    {
        public const string WerewolfVictim = "WerewolfVictim";

        public StateNight(GameContext context) : base(GameTime.Night, context, voter => voter.IsAlive && voter.Role == GameRole.Wereworlf)
        {

        }
        public override void Enter()
        {
            Context.Bag[WerewolfVictim] = null;
            base.Enter();
        }

        protected override void OnVote(Player player, Player voteFor)
        {
            if(voteFor.Role==GameRole.Wereworlf)
            {
                throw new InvalidOperationException("Cannot kill werewolf at night");
            }
        }

        protected override GameTime? ValidateVotes(int electedPlayer, int voteCount1, int voteCount2)
        {
            if (voteCount2 == 0)
            {
                Context.Bag[WerewolfVictim] = electedPlayer;
                return GameTime.Dawn;
            }
            return null;
        }
    }
}
