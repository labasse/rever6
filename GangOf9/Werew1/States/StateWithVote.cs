﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GangOf9.Werew1.States
{
    abstract class StateWithVote : State
    {
        private Dictionary<int, Vote> _votes = new Dictionary<int, Vote>();
        private int _votersCount;
        private readonly Func<IFullPlayer, bool> _voterSelector;

        public StateWithVote(GameTime phase, GameContext context, Func<IFullPlayer, bool> voterSelector) : base(phase, context)
        {
            _voterSelector = voterSelector;
        }

        protected abstract GameTime? ValidateVotes(int electedPlayer, int voteCount1, int voteCount2);
        protected virtual void OnVote(Player player, Player voteFor) { }

        public override void Enter()
        {
            _votes.Clear();
            _votersCount = Context.Players.Count(_voterSelector);
            foreach (var player in Context.Players)
            {
                if (player.IsAlive)
                {
                    _votes[player.Index] = new Vote();
                }
            }
        }
        public override int WaitedPlayerNum => _votersCount - _votes.Count(p => p.Value.HasVoted);

        public override IFullPlayer Join(string name) => throw new InvalidOperationException("Cannot join a started game");

        public override GameRole? Play(Turn t)
        {
            var from = Context.Players[t.Player];
            var vote = Context.Players[t.VoteFor];

            if (!_voterSelector(from))
            {
                throw new InvalidOperationException("Voter doesn't match criteria");
            }
            if (_votes[t.Player].HasVoted)
            {
                throw new InvalidOperationException("Already vote this turn");
            }
            if (!vote.IsAlive)
            {
                throw new InvalidOperationException("Cannot vote for a dead player");
            }

            OnVote((Player)from, (Player)vote);
            _votes[t.Player].DoVote();
            _votes[t.VoteFor].Inc();
            if (WaitedPlayerNum == 0)
            {
                var result = _votes.OrderBy(p => -p.Value.Counter).Take(2);
                var first = result.ElementAt(0);
                var nextPhase = ValidateVotes(first.Key, first.Value.Counter, result.Count() > 1 ? result.ElementAt(1).Value.Counter : 0);

                if (nextPhase.HasValue)
                {
                    Context.ChangeState(nextPhase.Value);
                }
                else
                {
                    _votes.Clear();
                }
            }
            return null;
        }
        private class Vote
        {
            public bool HasVoted { get; private set; } = false;
            public int Counter { get; private set; } = 0;

            public void DoVote() => HasVoted = true;
            public void Inc() => Counter++;

        }
    }
}
