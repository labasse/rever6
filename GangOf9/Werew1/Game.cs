﻿using GangOf9.Werew1.States;
using System;
using System.Collections.Generic;
using System.Text;

namespace GangOf9.Werew1
{
    public class Game
    {
        private GameContext _context;

        public Game()
        {
            _context = new GameContext();
            _context.Register(new StateLobby(_context));
            _context.Register(new StateNight(_context));
            _context.Register(new StateDawn (_context));
            _context.Register(new StateDay  (_context));
            _context.Register(new StateWin  (GameTime.WereworlvesWin, _context));
            _context.Register(new StateWin  (GameTime.VillagersWin  , _context));
        }

        public IFullPlayer Join(string name) => _context.CurState.Join(name);
      
        public IEnumerable<IPublicPlayer> Players => _context.Players;

        public GameTime Phase => _context.CurState.Phase;

        public int WaitedPlayerNum => _context.CurState.WaitedPlayerNum;

        public GameRole? Play(Turn t) => _context.CurState.Play(t);
    }
}
