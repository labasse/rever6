﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GangOf9.Shi4me
{
    public class Game
    {
        private readonly int[,] Solution = {
        // 1 > Rock, Paper, Scissors  // v 2
            {    0,     1,     2   }, // Rock
            {    2,     0,     1   }, // Paper
            {    1,     2,     0   }  // Scissors
        };
        private Objects?[] _choices = { null, null };
        private int[] _scores = { 0, 0 };

        public int? Winner { get; private set; }
        public int TurnNum { get; private set; }
        public int WaitedPlayerNum => _choices.Count(choice => choice==null);
        public int Score1 => _scores[0];
        public int Score2 => _scores[1];
        public void Play(Turn t)
        {
            if(t.Player<1 || 2<t.Player)
            {
                throw new ArgumentOutOfRangeException("Player number must be 1 or 2");
            }
            if(_choices[t.Player-1].HasValue)
            {
                throw new InvalidOperationException("Player already played");
            }
            _choices[t.Player - 1] = t.Choice;
            if(_choices.All(c => c.HasValue))
            {
                var result = Solution[(int)_choices[1].Value, (int)_choices[0].Value];

                if(result>0)
                {
                    Winner = result;
                    _scores[result - 1]++;
                }
                _choices[0] = _choices[1] = null;
                TurnNum++;
            }
            else
            {
                Winner = null;
            }
        }
    }
}
