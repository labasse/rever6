﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Rever6
{
    public struct Position
    {
        public const int MaxSize = 8;

        private static void CheckColumnRow(int colrow)
        {
            if (colrow < 0 || MaxSize <= colrow)
            {
                throw new ArgumentOutOfRangeException("Column/Row out of bounds");
            }
        }
        public Position(int col, int row)
        {
            CheckColumnRow(col);
            CheckColumnRow(row);
            Column = col;
            Row = row;
        }
        public int Column { get; private set; }
        public int Row { get; private set; }

        public bool Move(int dx, int dy)
        {
            var newCol = Column + dx;
            var newRow = Row    + dy;
            var valid = 0 <= newCol && newCol < MaxSize
                     && 0 <= newRow && newRow < MaxSize;
            
            if(valid)
            {
                Column = newCol;
                Row    = newRow;
            }
            return valid;
        }
        
        public bool Next()
        {
            if(Column == MaxSize - 1)
            {
                if(Row == MaxSize - 1)
                {
                    return false;
                }
                Row++;
                Column = -1;
            }
            Column++;
            return true;
        }

        public static Position Parse(string textWithPosition)
        {
            if(textWithPosition==null)
            {
                throw new ArgumentNullException("Text to parse cannot be null");
            }
            if(textWithPosition.Length!=2)
            {
                throw new ArgumentException("Invalid position format");
            }
            if(textWithPosition[0] < 'A' || 'Z' < textWithPosition[0])
            {
                throw new ArgumentException("Column must be a letter");
            }
            var col = textWithPosition[0] - 'A';
            var row = textWithPosition[1] - '1';

            return new Position(col, row);
        }

        public override string ToString() => $"{GetColumnName(Column)}{GetRowName(Row)}";

        public override bool Equals(object obj) => obj is Position && this == (Position)obj;

        public override int GetHashCode() => ToString().GetHashCode();

        public static bool operator == (Position a, Position b) => a.Column == b.Column && a.Row == b.Row;

        public static bool operator !=(Position a, Position b) => !(a == b);
        
        public static string GetColumnName(int col)
        {
            CheckColumnRow(col);
            return $"{(char)('A' + col)}";
        }

        public static string GetRowName(int row)
        {
            CheckColumnRow(row);
            return $"{ row + 1 }";
        }
    }
}
