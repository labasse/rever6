﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Rever6
{
    public class Game
    {
        public const int Size = 8;
        public const int Empty = 0;
        public const int Black = 1;
        public const int White = -1;

        private int[,] _board = new int[Size, Size];
        public Game()
        {
            _board[Size / 2 - 1, Size / 2 - 1] = _board[Size / 2, Size / 2] = Black;
            _board[Size / 2 - 1, Size / 2] = _board[Size / 2, Size / 2 - 1] = White;
            CurPlayer = Black;
        }
        /// <summary>
        /// Initialize a game with the given board and first player.
        /// </summary>
        /// <param name="board">List of tile states from left to right and top to bottom.</param>
        /// <param name="curPlayer">The player to start with. If it cannot play, the other player is selected in CurPlayer.</param>
        public Game(IEnumerable<int> board, int curPlayer)
        {
            var pos = new Position();

            foreach (var tile in board)
            {
                SetTile(pos, tile);
                if(!pos.Next())
                {
                    break;
                }
            }
            FindNextPlayer(curPlayer);
        }
        public int GetTile(int col, int row) => _board[col, row];
        
        public int GetTile(Position pos) => _board[pos.Column, pos.Row];
        
        private void SetTile(Position pos, int value)
        {
            _board[pos.Column, pos.Row] = value;
        }
        /// <summary>
        /// Color of the current player. null if no player can play (end of the game)
        /// </summary>
        public int? CurPlayer { get; private set; } = Black;

        /// <summary>
        /// Winner of the game. If null, no winner yet.
        /// </summary>
        public int? Winner { get; private set; } = null;

        /// <summary>
        /// Gets all the board squares starting from left to right and then from top to bottom
        /// </summary>
        public IEnumerable<int> Tiles
        {
            get
            {
                // return _board; Impossible car int [,] n'est pas IEnumerable<int>
                for (var row = 0; row < Size; row++)
                {
                    for (var col = 0; col < Size; col++)
                    {
                        yield return _board[col, row]; // yield retourne le 1er, puis le second (n'interrompt pas le foreach)
                    }
                }
            }
        }

        /// <summary>
        /// Put a token of the current player at the given position. CurPlayer change to the next player.
        /// </summary>
        /// <param name="pos">The position to play on</param>
        /// <returns>The number of token reversed</returns>
        /// <exception cref="System.InvalidOperationException">If a token is already on this square or if no revert can be done</exception>
        public int Play(Position pos)
        {
            if (_board[pos.Column, pos.Row] != Empty)
            {
                throw new InvalidOperationException("Already played here");
            }
            else if (!CurPlayer.HasValue)
            {
                throw new InvalidOperationException("Cannot play on an ended game");
            }
            var turn = LookAround(CurPlayer.Value, pos, true);
            
            if(turn == null) 
            {
                throw new InvalidOperationException("Bad tile : Must revert tokens");
            }
            _history.Add(turn);
            FindNextPlayer(-CurPlayer.Value);
            return turn.Score;
        }
        public struct PlayLocation
        {
            public Position Pos { get; set; }
            public int Score { get; set; }

        };
        public int Play(Rever6.Turn turn) => Play(new Position(turn.Column, turn.Row));

        public IEnumerable<PlayLocation> CurPlayerPossibilities =>
            CurPlayer.HasValue
            ? WhereCanIPlay(CurPlayer.Value)
            : new PlayLocation[0];

        /// <summary>
        /// Get the positions and score where a given player can play.
        /// </summary>
        /// <param name="player">The player used to find the possible playing positions</param>
        /// <returns>A list of pair of position/score found. Empty if the player cannot play.</returns>
        public IEnumerable<PlayLocation> WhereCanIPlay(int player)
        {
            var pos = new Position();

            do
            {
                if (GetTile(pos) == Empty)
                {
                    var turn = LookAround(player, pos, false);

                    if(turn!=null)
                    {
                        yield return new PlayLocation() { Pos = pos, Score = turn.Score };
                    }                    
                }
            }
            while (pos.Next());
        }

        /// <summary>
        /// Get the tile number for a given player
        /// </summary>
        /// <param name="player">Player whose tiles must be counted</param>
        /// <returns>Number of tile owned by the player</returns>
        public int GetScore(int player) => Tiles.Count(tile => tile==player);

        public IEnumerable<(Position pos, int color, int score)> History
        {
            get
            {
                foreach (var turn in _history)
                {
                    yield return (turn.Pos, turn.Player, turn.Score);
                }
            }
        }

        public void Undo() 
        {
            if(_history.Count==0)
            {
                throw new InvalidOperationException("Unable to undo an empty history");
            }
            var last = _history.Count - 1;
            var turn = _history[last];
            
            _history.RemoveAt(last);
            foreach(var change in turn.Changes)
            {
                var pos = turn.Pos;
                var dir = delta[change.direction];
                for (int i=0; i<change.distance; i++)
                {
                    pos.Move(dir.dx, dir.dy);
                    SetTile(pos, -GetTile(pos));
                }
            }
            SetTile(turn.Pos, Empty);
        }

        #region Private
        private void FindNextPlayer(int tryPlayer)
        {
            if (WhereCanIPlay(tryPlayer).Any()) { CurPlayer = tryPlayer; }
            else if (WhereCanIPlay(-tryPlayer).Any()) { CurPlayer = -tryPlayer; }
            else
            {
                var total = Tiles.Sum();

                CurPlayer = null;
                Winner = (total == 0)
                    ? (int?)null
                    : (total < 0 ? White : Black);
            }
        }

        private readonly (int dx, int dy)[] delta = new (int dx, int dy)[]{
            ( -1, -1 ), (  0, -1 ), (  1, -1 ),
            ( -1,  0 ),             (  1,  0 ),
            ( -1,  1 ), (  0,  1 ), (  1,  1 )
        };


        private Turn LookAround(int player, Position pos, bool apply)
        {
            var otherColor = -player;
            Turn turn = null;

            for (var dir = 0; dir < delta.Length; dir++)
            {
                var distance = BrowseTiles(pos, delta[dir].dx, delta[dir].dy, otherColor, apply);

                if (distance > 0)
                {
                    if (turn == null)
                    {
                        turn = new Turn(pos, player);
                        if (apply)
                        {
                            SetTile(pos, CurPlayer.Value);
                        }
                    }
                    turn.Add(direction: dir, distance: distance);
                }
            }
            return turn;
        }

        private int BrowseTiles(Position pos, int dx, int dy, int color, bool apply, int distance = 0)
        {
            if (!pos.Move(dx, dy) || GetTile(pos) == Empty)
            {
                return 0;
            }
            else if(GetTile(pos) != color) 
            {
                return distance; 
            }
            else 
            {
                var score = BrowseTiles(pos, dx, dy, color, apply, distance + 1);

                if(apply && score > 0)
                {
                    SetTile(pos, -GetTile(pos));
                }
                return score;
            }
        }

        #endregion

        #region History
        private class Turn
        {
            private List<(int direction, int distance)> _changes = new List<(int, int)>();

            public Turn(Position pos, int player)
            {
                Pos = pos;
                Player = player;
            }

            public Position Pos { get; private set; }
            public int Player { get; private set; }

            public int Score => _changes.Sum(pair => pair.distance);

            public void Add(int direction, int distance) => _changes.Add((direction, distance));

            public IEnumerable<(int direction, int distance)> Changes => _changes;
        }
        private List<Turn> _history = new List<Turn>();
        #endregion
    }
}
