﻿using NUnit.Framework;
using Rever6;
using System;
using System.Linq;
using System.Collections.Generic;

namespace TestRever6
{
    public class TestGame
    {
        private const int _ = 0, O = -1, X = 1;

        #region Init Game
        [Test]
        public void InitGameWithStartingState()
        {
            var test = new Game();

            Assert.AreEqual(Game.Black, test.CurPlayer.Value);
            Assert.IsFalse(test.Winner.HasValue);
            Assert.AreEqual(2, test.GetScore(Game.Black));
            Assert.AreEqual(2, test.GetScore(Game.White));
            CollectionAssert.AreEqual(
                new int[] {
                //  A  B  C  D  E  F  G  H
                    _, _, _, _, _, _, _, _, // 1
                    _, _, _, _, _, _, _, _, // 2
                    _, _, _, _, _, _, _, _, // 3
                    _, _, _, X, O, _, _, _, // 4
                    _, _, _, O, X, _, _, _, // 5
                    _, _, _, _, _, _, _, _, // 6
                    _, _, _, _, _, _, _, _, // 7
                    _, _, _, _, _, _, _, _  // 8
                },
                test.Tiles
            );
        }
        [Test]
        public void InitGameWithSpecificBoard()
        {
            // https://youtu.be/Z5EN-cbgo-4?t=123
            var state = new int[] {
            //  A  B  C  D  E  F  G  H   
                _, _, _, _, _, _, _, _, // 1
                _, _, _, _, _, _, _, _, // 2
                _, _, _, _, _, _, _, _, // 3
                _, _, _, _, O, X, _, _, // 4
                _, O, O, O, O, O, _, _, // 5
                _, X, _, O, _, _, _, _, // 6
                _, _, _, X, _, _, _, _, // 7
                _, _, _, _, _, _, _, _  // 8
            };
            var test = new Game(state, Game.Black);

            Assert.AreEqual(Game.Black, test.CurPlayer.Value);
            Assert.IsFalse(test.Winner.HasValue);
            Assert.AreEqual(3, test.GetScore(Game.Black));
            Assert.AreEqual(7, test.GetScore(Game.White));
            CollectionAssert.AreEqual(state, test.Tiles);
        }
        [Test]
        public void InitGameWithSpecificBoardAndBlockedPlayer()
        {
            // https://youtu.be/Z5EN-cbgo-4?t=221
            var state = new int[] {
            //  A  B  C  D  E  F  G  H   
                _, _, _, _, _, _, _, _, // 1
                _, _, _, _, _, _, _, _, // 2
                _, _, _, O, _, _, _, _, // 3
                _, _, O, O, O, _, _, _, // 4
                O, O, O, O, O, O, _, _, // 5
                _, O, O, O, _, _, _, _, // 6
                X, X, O, O, O, _, _, _, // 7
                X, X, X, O, _, _, _, _  // 8
            };
            var test = new Game(state, Game.White);

            Assert.AreEqual(Game.Black, test.CurPlayer.Value);
            Assert.IsFalse(test.Winner.HasValue);
            CollectionAssert.AreEqual(state, test.Tiles);
        }
        [Test]
        public void InitGameWithATerminatedGame()
        {
            // https://youtu.be/Z5EN-cbgo-4?t=278
            var state = new int[] {
            //  A  B  C  D  E  F  G  H   
                O, O, O, O, O, O, O, O, // 1
                O, O, X, X, X, O, O, O, // 2
                O, X, O, X, X, X, O, O, // 3
                O, X, O, O, X, O, O, O, // 4
                O, X, O, X, O, O, O, O, // 5
                O, X, O, X, O, O, O, O, // 6
                O, O, X, X, X, O, O, O, // 7
                O, X, X, X, X, X, X, X  // 8
            };
            var test = new Game(state, Game.Black);

            Assert.IsFalse(test.CurPlayer.HasValue);
            Assert.AreEqual(Game.White, test.Winner.Value);
        }
        #endregion

        #region WhereCanIPlay
        [Test]
        public void TypicalWhereCanIPlay()
        {
            var state = new int[] {
            //  A  B  C  D  E  F  G  H   
                _, _, _, _, _, _, _, _, // 1
                _, _, _, X, _, _, _, _, // 2
                _, X, _, O, _, X, _, _, // 3
                _, O, O, O, O, O, _, _, // 4
                _, X, O, _, O, X, _, _, // 5
                _, _, O, O, O, _, _, _, // 6
                _, X, _, X, _, X, _, _, // 7
                _, _, _, _, _, _, _, _  // 8
            };
            var test = new Game(state, Game.Black);

            CollectionAssert.AreEqual(new (Position, int)[] {
                (Position.Parse("C2"), 2),
                (Position.Parse("E2"), 2),
                (Position.Parse("D5"), 9)
            }, test.WhereCanIPlay(Game.Black));
        }
        public void WhereCanIPlayEmpty()
        {
            // https://youtu.be/Z5EN-cbgo-4?t=221
            var state = new int[] {
            //  A  B  C  D  E  F  G  H   
                _, _, _, _, _, _, _, _, // 1
                _, _, _, _, _, _, _, _, // 2
                _, _, _, O, _, _, _, _, // 3
                _, _, O, O, O, _, _, _, // 4
                O, O, O, O, O, O, _, _, // 5
                _, O, O, O, _, _, _, _, // 6
                X, X, O, O, O, _, _, _, // 7
                X, X, X, O, _, _, _, _  // 8
            };
            var test = new Game(state, Game.White);

            CollectionAssert.AreEqual(new (Position, int)[0], test.WhereCanIPlay(Game.White));
        }
        #endregion

        #region Play
        [Test]
        public void PlayAValidPosition()
        {
            var test = new Game();

            Assert.AreEqual(1, test.Play(Position.Parse("F4")));
            Assert.AreEqual(Game.White, test.CurPlayer.Value);
            Assert.AreEqual(4, test.GetScore(Game.Black));
            Assert.AreEqual(1, test.GetScore(Game.White));
            CollectionAssert.AreEqual(
                new int[] {
                //  A  B  C  D  E  F  G  H
                    _, _, _, _, _, _, _, _, // 1
                    _, _, _, _, _, _, _, _, // 2
                    _, _, _, _, _, _, _, _, // 3
                    _, _, _, X, X, X, _, _, // 4
                    _, _, _, O, X, _, _, _, // 5
                    _, _, _, _, _, _, _, _, // 6
                    _, _, _, _, _, _, _, _, // 7
                    _, _, _, _, _, _, _, _  // 8
                },
                test.Tiles
            );
        }
        [Test]
        public void PlayTheUpperLeftPosition()
        {
            var test = new Game(new int[] {
            //  A  B  C  D  E  F  G  H
                _, X, O, _, _, _, _, _, // 1
                _, _, _, _, _, _, _, _, // 2
                _, _, _, _, _, _, _, _, // 3
                _, _, _, _, _, _, _, _, // 4
                _, _, _, _, _, _, _, _, // 5
                _, _, _, _, _, _, _, _, // 6
                _, _, _, _, _, _, _, _, // 7
                _, _, _, _, _, _, _, _  // 8
            }, Game.White);

            Assert.AreEqual(1, test.Play(Position.Parse("A1")));
            CollectionAssert.AreEqual(
                new int[] {
                //  A  B  C  D  E  F  G  H
                    O, O, O, _, _, _, _, _, // 1
                    _, _, _, _, _, _, _, _, // 2
                    _, _, _, _, _, _, _, _, // 3
                    _, _, _, _, _, _, _, _, // 4
                    _, _, _, _, _, _, _, _, // 5
                    _, _, _, _, _, _, _, _, // 6
                    _, _, _, _, _, _, _, _, // 7
                    _, _, _, _, _, _, _, _  // 8
                },
                test.Tiles
            );
        }
        [Test]
        public void PlayAValidPositionMultipleChanges()
        {
            var state = new int[] {
            //  A  B  C  D  E  F  G  H   
                _, _, _, _, _, _, _, _, // 1
                _, _, _, X, _, _, _, _, // 2
                _, X, _, O, _, X, _, _, // 3
                _, O, O, O, O, O, _, _, // 4
                _, X, O, _, O, X, _, _, // 5
                _, _, O, O, O, _, _, _, // 6
                _, X, _, X, _, X, _, _, // 7
                _, _, _, _, _, _, _, _  // 8
            };
            var test = new Game(state, Game.Black);

            Assert.AreEqual(9, test.Play(Position.Parse("D5")));
            Assert.AreEqual(Game.White, test.CurPlayer.Value);
            Assert.IsFalse(test.Winner.HasValue);
            CollectionAssert.AreEqual(new int[] {
            //  A  B  C  D  E  F  G  H   
                _, _, _, _, _, _, _, _, // 1
                _, _, _, X, _, _, _, _, // 2
                _, X, _, X, _, X, _, _, // 3
                _, O, X, X, X, O, _, _, // 4
                _, X, X, X, X, X, _, _, // 5
                _, _, X, X, X, _, _, _, // 6
                _, X, _, X, _, X, _, _, // 7
                _, _, _, _, _, _, _, _  // 8
            }, test.Tiles);
        }
        [Test]
        public void PlayTheLastTurn()
        {
            // https://youtu.be/Z5EN-cbgo-4?t=278
            var state = new int[] {
            //  A  B  C  D  E  F  G  H   
                O, O, O, O, O, O, O, O, // 1
                O, O, X, X, X, O, O, O, // 2
                O, X, O, X, X, X, O, O, // 3
                O, X, O, O, X, O, O, O, // 4
                O, X, O, X, O, O, O, O, // 5
                O, X, O, X, O, O, O, O, // 6
                X, X, X, X, X, O, O, O, // 7
                _, X, X, X, X, X, X, X  // 8
            };
            var test = new Game(state, Game.White);

            Assert.AreEqual(2, test.Play(Position.Parse("A8")));
            Assert.IsFalse(test.CurPlayer.HasValue);
            Assert.AreEqual(Game.White, test.Winner.Value);
            CollectionAssert.AreEqual(new int[] {
            //  A  B  C  D  E  F  G  H   
                O, O, O, O, O, O, O, O, // 1
                O, O, X, X, X, O, O, O, // 2
                O, X, O, X, X, X, O, O, // 3
                O, X, O, O, X, O, O, O, // 4
                O, X, O, X, O, O, O, O, // 5
                O, X, O, X, O, O, O, O, // 6
                O, O, X, X, X, O, O, O, // 7
                O, X, X, X, X, X, X, X  // 8
            }, test.Tiles);
        }
        [Test]
        public void EndOfGameAsADraw()
        {
            var state = new int[] {
            //  A  B  C  D  E  F  G  H          O   X
                O, O, O, O, O, O, O, O, // 1 :  8 > 0
                O, X, O, O, X, O, O, X, // 2 :  5 > 3
                O, X, O, O, X, X, O, X, // 3 :  4 = 4
                O, X, X, O, X, O, O, X, // 4 :  4 = 4
                O, X, O, X, O, O, O, X, // 5 :  4 = 4
                O, X, _, O, X, O, O, O, // 6 :  5 > 2
                O, X, X, X, X, O, O, X, // 7 :  3 < 5
                X, X, X, X, X, X, X, X  // 8 :  0 < 8
            };
            var test = new Game(state, Game.Black);

            Assert.AreEqual(2, test.Play(Position.Parse("C6")));
            Assert.IsFalse(test.CurPlayer.HasValue);
            Assert.IsFalse(test.Winner.HasValue);
            CollectionAssert.AreEqual(new int[] {
            //  A  B  C  D  E  F  G  H          O   X
                O, O, O, O, O, O, O, O, // 1 :  8 > 0
                O, X, O, O, X, O, O, X, // 2 :  5 > 3
                O, X, O, O, X, X, O, X, // 3 :  4 = 4
                O, X, X, O, X, O, O, X, // 4 :  4 = 4
                O, X, X, X, O, O, O, X, // 5 :  4 = 4
                O, X, X, X, X, O, O, O, // 6 :  4 = 4
                O, X, X, X, X, O, O, X, // 7 :  3 < 5
                X, X, X, X, X, X, X, X  // 8 :  0 < 8
            }, test.Tiles);
        }
        #endregion

        #region Play errors
        [Test]
        public void PlayOnATerminatedGame()
        {
            var state = new int[] {
            //  A  B  C  D  E  F  G  H      
                X, O, O, O, O, O, O, O, // 1
                X, X, O, O, X, O, O, X, // 2
                X, X, O, O, X, X, O, X, // 3
                X, X, X, O, X, O, O, X, // 4
                X, X, X, X, O, O, O, X, // 5
                X, X, X, X, X, O, O, O, // 6
                X, X, X, X, X, O, O, X, // 7
                _, X, X, X, X, X, X, X  // 8
            };
            var test = new Game(state, Game.Black);

            Assert.Throws<InvalidOperationException>(() => test.Play(Position.Parse("A1")));
        }
        [Test]
        public void PlayAnAlreadyPlayedPosition()
        {
            var test = new Game();

            Assert.Throws<InvalidOperationException>(() => test.Play(Position.Parse("D4")));
        }
        [Test]
        public void PlayAnInvalidPosition()
        {
            var test = new Game();

            Assert.Throws<InvalidOperationException>(() => test.Play(Position.Parse("F5")));
        }
        [Test]
        public void PlayAnIsolatedPosition()
        {
            var test = new Game();

            Assert.Throws<InvalidOperationException>(() => test.Play(Position.Parse("B5")));
        }
        #endregion

        #region History/Undo
        [Test]
        public void PlayOnceKeepHistory()
        {
            var state = new int[] {
            //  A  B  C  D  E  F  G  H   
                _, _, _, _, _, _, _, _, // 1
                _, _, _, X, _, _, _, _, // 2
                _, X, _, O, _, X, _, _, // 3
                _, O, O, O, O, O, _, _, // 4
                _, X, O, _, O, X, _, _, // 5
                _, _, O, O, O, _, _, _, // 6
                _, X, _, X, _, X, _, _, // 7
                _, _, _, _, _, _, _, _  // 8
            };
            var test = new Game(state, Game.Black);

            test.Play(Position.Parse("D5"));
            Assert.AreEqual(new (Position, int, int)[] {
                (Position.Parse("D5"), Game.Black, 9)
            }, test.History);
        }
        [Test]
        public void PlayTwiceKeepHistory()
        {
            var test = new Game();
            var initialState = test.Tiles.ToArray();

            test.Play(Position.Parse("F4"));
            test.Play(Position.Parse("D3"));
            Assert.AreEqual(new (Position, int, int)[] {
                (Position.Parse("F4"), Game.Black, 1),
                (Position.Parse("D3"), Game.White, 1)
            }, test.History);
        }
        public void PlayOnceAndUndoReturnToInitialState()
        {
            var state = new int[] {
            //  A  B  C  D  E  F  G  H   
                _, _, _, _, _, _, _, _, // 1
                _, _, _, X, _, _, _, _, // 2
                _, X, _, O, _, X, _, _, // 3
                _, O, O, O, O, O, _, _, // 4
                _, X, O, _, O, X, _, _, // 5
                _, _, O, O, O, _, _, _, // 6
                _, X, _, X, _, X, _, _, // 7
                _, _, _, _, _, _, _, _  // 8
            };
            var test = new Game(state, Game.Black);

            test.Play(Position.Parse("D5"));
            test.Undo();
            CollectionAssert.AreEqual(state, test.Tiles);
        }
        [Test]
        public void PlayTwiceAndUndoReturnToInitialState()
        {
            var test = new Game();
            var initialState = test.Tiles.ToArray();

            test.Play(Position.Parse("F4"));
            test.Play(Position.Parse("D3"));
            test.Undo();
            test.Undo();
            CollectionAssert.AreEqual(initialState, test.Tiles);
        }
        [Test]
        public void UndoTwiceWhileNotPossible()
        {
            var test = new Game();

            test.Play(Position.Parse("F4"));
            test.Undo();
            Assert.Throws<InvalidOperationException>(() => test.Undo());
        }
        #endregion
    }
}