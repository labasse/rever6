using NUnit.Framework;
using Rever6;
using System;

namespace TestRever6
{
    public class TestPosition
    {
        #region Constructor
        [Test]
        public void InitPositionWithValidPosition()
        {
            var test = new Position(col: 3, row: 4);

            Assert.AreEqual(3, test.Column);
            Assert.AreEqual(4, test.Row);
        }
        [Test]
        public void InitPositionWithUpperLeft()
        {
            var test = new Position(col: 0, row: 0);

            Assert.AreEqual(0, test.Column);
            Assert.AreEqual(0, test.Row);
        }
        [Test]
        public void InitPositionWithBottomRight()
        {
            var test = new Position(col: Position.MaxSize - 1, row: Position.MaxSize - 1);

            Assert.AreEqual(7, test.Column);
            Assert.AreEqual(7, test.Row);
        }
        [Test]
        public void InitPositionWithInvalidColumn()
        {
            Assert.Throws<ArgumentOutOfRangeException>(() => new Position(col: -1, row: 4));
        }
        [Test]
        public void InitPositionWithInvalidRow()
        {
            Assert.Throws<ArgumentOutOfRangeException>(() => new Position(col: 3, row: Position.MaxSize));
        }
        #endregion

        #region Object overrides and Equality
        [Test]
        public void OperatorEqual()
        {
            var test1 = Position.Parse("A1");
            var test2 = new Position();

            Assert.IsTrue(test1==test2);
            Assert.IsTrue(test1.Equals(test2));
            Assert.IsTrue(test1.GetHashCode() == test2.GetHashCode());
        }
        [Test]
        public void OperatorDifferent()
        {
            var test1 = Position.Parse("A1");
            var test2 = new Position(5, 3);

            Assert.IsTrue(test1 != test2);
            Assert.IsFalse(test1.Equals(test2));
            Assert.IsTrue(test1.GetHashCode() != test2.GetHashCode());
        }
        [Test]
        public void ToStringA1()
        {
            var test = new Position();

            Assert.AreEqual("A1", test.ToString());
        }
        [Test]
        public void ToStringF6()
        {
            var test = Position.Parse("F6");

            Assert.AreEqual("F6", test.ToString());
        }
        #endregion

        #region Next
        [Test]
        public void NextFromStart()
        {
            var test = new Position();

            Assert.IsTrue(test.Next());
            Assert.AreEqual(1, test.Column);
            Assert.AreEqual(0, test.Row);
        }
        [Test]
        public void NextAtEndOfLine()
        {
            var test = new Position(Position.MaxSize - 1, 2);

            Assert.IsTrue(test.Next());
            Assert.AreEqual(0, test.Column);
            Assert.AreEqual(3, test.Row);
        }
        [Test]
        public void NextAtEndOfBoard()
        {
            var test = new Position(Position.MaxSize - 1, Position.MaxSize - 1);

            Assert.IsFalse(test.Next());
            Assert.AreEqual(Position.MaxSize - 1, test.Column);
            Assert.AreEqual(Position.MaxSize - 1, test.Row);
        }
        #endregion

        #region Move
        [Test]
        public void MovePositiveWithinTheBoard()
        {
            var test = new Position(5, 3);

            Assert.IsTrue(test.Move(2, 1));
            Assert.AreEqual(7, test.Column);
            Assert.AreEqual(4, test.Row);
        }
        [Test]
        public void MoveNegativeWithinTheBoard()
        {
            var test = new Position(5, 3);

            Assert.IsTrue(test.Move(-1, -2));
            Assert.AreEqual(4, test.Column);
            Assert.AreEqual(1, test.Row);
        }
        [Test]
        public void MovePositiveOverTheBoard()
        {
            var test = new Position(6, 3);

            Assert.IsFalse(test.Move(2, 1));
            Assert.AreEqual(6, test.Column);
            Assert.AreEqual(3, test.Row);
        }
        [Test]
        public void MoveNegativeOverTheBoard()
        {
            var test = new Position(0, 2);

            Assert.IsFalse(test.Move(-1, -2));
            Assert.AreEqual(0, test.Column);
            Assert.AreEqual(2, test.Row);
        }
        #endregion

        #region Parse method
        [Test]
        public void ParsePositionWithValidPosition()
        {
            var test = Position.Parse("C4");

            Assert.AreEqual(2, test.Column);
            Assert.AreEqual(3, test.Row);
        }
        [Test]
        public void ParsePositionWithNullString()
        {
            Assert.Throws<ArgumentNullException>(() => Position.Parse(null));
        }
        [Test]
        public void ParsePositionWithInvalidColumn()
        {
            Assert.Throws<ArgumentException>(() => Position.Parse("42"));
        }
        [Test]
        public void ParsePositionWithTooBigColumn()
        {
            Assert.Throws<ArgumentOutOfRangeException>(() => Position.Parse("X1"));
        }
        [Test]
        public void ParsePositionWithTooBigRow()
        {
            Assert.Throws<ArgumentOutOfRangeException>(() => Position.Parse("C9"));
        }
        #endregion

        #region GetRowName
        [Test]
        public void GetRowNameWithValidColumn()
        {
            Assert.AreEqual("3", Position.GetRowName(2));
        }
        [Test]
        public void GetRowNameWithLastColumn()
        {
            Assert.AreEqual("8", Position.GetRowName(Position.MaxSize - 1));
        }
        [Test]
        public void GetRowNameWithNegativeColumn()
        {
            Assert.Throws<ArgumentOutOfRangeException>(() => Position.GetRowName(-1));
        }
        [Test]
        public void GetRowNameWithInvalidColumn()
        {
            Assert.Throws<ArgumentOutOfRangeException>(() => Position.GetRowName(Position.MaxSize));
        }
        #endregion

        #region GetColumnName
        [Test]
        public void GetColumnNameWithValidColumn()
        {
            Assert.AreEqual("C", Position.GetColumnName(2));
        }
        [Test]
        public void GetColumnNameWithLastColumn()
        {
            Assert.AreEqual("H", Position.GetColumnName(Position.MaxSize - 1));
        }
        [Test]
        public void GetColumnNameWithNegativeColumn()
        {
            Assert.Throws<ArgumentOutOfRangeException>(() => Position.GetColumnName(-1));
        }
        [Test]
        public void GetColumnNameWithInvalidColumn()
        {
            Assert.Throws<ArgumentOutOfRangeException>(() => Position.GetColumnName(Position.MaxSize));
        }
        #endregion
    }
}