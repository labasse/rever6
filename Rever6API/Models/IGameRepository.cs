﻿using Rever6;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Rever6API.Models
{
    public interface IGameRepository
    {
        IDictionary<string, Game> AllGames { get; }

        Game FindByKey(string key);
    }
}
