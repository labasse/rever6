﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Rever6;
using Rever6API.Models;

namespace Rever6API.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class GamesController : ControllerBase
    {
        private readonly ILogger<GamesController> _logger;
        private IGameRepository _repoGame;

        public GamesController(IGameRepository repoGame, ILogger<GamesController> logger)
        {
            _logger = logger;
            _repoGame = repoGame;
        }

        [HttpGet]
        public IDictionary<string, Game> Get()
        {
            return _repoGame.AllGames;
        }
        [HttpGet("{key}")]
        public Game Get(string key)
        {
            return _repoGame.FindByKey(key);
        }
        [HttpPatch("{key}")]
        public ActionResult<Game> Patch(string key, [FromBody] Turn turn)
        {
            var game = _repoGame.FindByKey(key);

            try
            {
                game.Play(turn);
                return game;
            }
            catch (InvalidOperationException e)
            {
                return BadRequest(e.Message);
            }
            catch (ArgumentException e)
            {
                return BadRequest(e.Message);
            }
        }

    }
}
